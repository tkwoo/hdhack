#include <iostream>
#include <stdlib.h>
#include <winsock2.h>

#include <opencv2\opencv.hpp>
#include <string>
#include <fstream>

#include "StereoVisionForADAS.h"
#include "IntegrationVD.h"

#pragma comment(lib,"ws2_32.lib")
#define ID 0691

using namespace std;
using namespace cv;

void ErrorHandling(char *message);

StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);

int nScale = 10;
void CollisionRisk(Mat& imgTopView, vector<SObjectHD>& objObjectHD, int nVelo = 50){
	for (int i = 0; i < objObjectHD.size(); i++){
		if ((objObjectHD[i].nClass == Car && objObjectHD[i].dX < 1.0 && objObjectHD[i].dX > -3.0) || nVelo > 70){
			//cout << objObjectHD[i].dZ << endl;
			if ((nVelo*0.28)*(nVelo*0.28) / (254 * 0.0409) > objObjectHD[i].dZ || nVelo > 70){
				putText(imgTopView, "!!!WARNING!!!", Point(40, 300), FONT_HERSHEY_COMPLEX, 2, Scalar(0, 0, 255), 4);
			}
		}
	}
}
void TopView(Mat& imgTopView, vector<SObjectHD>& objDetectedObject, Mat& imgGround, int nVelo = 10){
	Mat imgCarTopView = imread("./image/car_topview.png", 1);
	Mat imgCarTopView2 = imread("./image/car_topview_cut.png", 1);
	for (int i = 0; i < imgCarTopView2.rows; i++){
		for (int j = 0; j < imgCarTopView2.cols; j++){
			Vec3b clTemp = imgCarTopView2.at<Vec3b>(i, j);
			if (clTemp(1) > 150 || clTemp(2) > 150) imgCarTopView2.at<Vec3b>(i, j) = 0;
			else imgCarTopView2.at<Vec3b>(i, j) += Vec3b(70,30,30);
		}
	}

	imgTopView.setTo(0);
	
	for (int i = 0; i < imgGround.rows; i++){
		for (int j = 0; j < imgGround.cols; j++){
			double dDispTemp = (double)imgGround.at<uchar>(i, j)*objParam.m_nNumberOfDisp / 255;
			if (dDispTemp < 2) continue;
			double dZ = objParam.m_dBaseLine*objParam.objCamParam.m_dFocalLength / dDispTemp;
			double dX = (objParam.objCamParam.m_sizeSrc.width / 2 - j)*dZ / objParam.objCamParam.m_dFocalLength;
			if (dZ > 50 || abs(dX) > 25) continue;
			imgTopView.at<Vec3b>(500 - nScale*dZ, 250 - nScale*dX) = Vec3b(100, 250, 150);
		}
	}

	Scalar clStandard(150, 150, 150);

	line(imgTopView, Point(0, 100), Point(imgTopView.cols, 100), clStandard);
	line(imgTopView, Point(0, 200), Point(imgTopView.cols, 200), clStandard);
	line(imgTopView, Point(78, 300), Point(imgTopView.cols - 78, 300), clStandard);
	line(imgTopView, Point(164, 400), Point(imgTopView.cols - 164, 400), clStandard);

	for (int i = 0; i <= 500; i += 100) {
		char chTemp[20];
		sprintf(chTemp, "%dm", 50 - i / 10);
		putText(imgTopView, chTemp, Point(imgTopView.cols - 25, i + 12), FONT_HERSHEY_PLAIN, 0.7, Scalar(255, 255, 255), 1);
	}

	line(imgTopView, Point(250, 500), Point(500, 500 - 290), Scalar(0, 255, 255), 2);
	line(imgTopView, Point(250, 500), Point(0, 500 - 290), Scalar(0, 255, 255), 2);

	line(imgTopView, Point(250 - 20, 0), Point(250 - 20, 477), Scalar::all(255), 2);
	line(imgTopView, Point(250 + 20, 0), Point(250 + 20, 477), Scalar::all(255), 2);

	Mat imgPartCenterCar = imgTopView(Rect(215, 490, 70, 70));
	resize(imgCarTopView, imgCarTopView, imgPartCenterCar.size());
	//imshow("hi", imgCarTopView);
	for (int i = 0; i < imgPartCenterCar.rows; i++){
		for (int j = 0; j < imgPartCenterCar.cols; j++){
			Vec3b clTemp = imgCarTopView.at<Vec3b>(i, j);
			if (clTemp(1) < 150 || clTemp(2) < 150) imgPartCenterCar.at<Vec3b>(i, j) = clTemp;
		}
	}

	for (int i = 0; i < objDetectedObject.size(); i++){
		Point ptTemp(250 - nScale * objDetectedObject[i].dX, 500 - nScale * objDetectedObject[i].dZ);
		if (objDetectedObject[i].nClass == Car){
			Mat imgCar = imgTopView(Rect(ptTemp.x - 5, ptTemp.y - 70, 35, 70));
			line(imgTopView, ptTemp, ptTemp + Point(nScale * objDetectedObject[i].dWidth, 0), Scalar(255, 0, 255), 3);
			resize(imgCarTopView2, imgCar, imgCar.size());
		}
		else
			line(imgTopView, ptTemp, ptTemp + Point(nScale * objDetectedObject[i].dWidth, 0), Scalar(0, 0, 255), 3);
	}
	CollisionRisk(imgTopView, objDetectedObject, nVelo);
}
void cvtStixelObj2HDobj(vector<Object_t>& objDetectedObject, vector<SObjectHD>& objObjectHD){
	for (int i = 0; i < objDetectedObject.size(); i++){
		SObjectHD objTemp;
		objTemp.dZ = objDetectedObject[i].dZ;
		objTemp.dX = (objParam.objCamParam.m_sizeSrc.width / 2 - objDetectedObject[i].rectBB.x)*objTemp.dZ / objParam.objCamParam.m_dFocalLength;
		objTemp.dWidth = objDetectedObject[i].rectBB.width*objTemp.dZ / objParam.objCamParam.m_dFocalLength;
		objTemp.nClass = objDetectedObject[i].nClass;
		objObjectHD.push_back(objTemp);
	}
}
void MakeStateMessage(SVehicleInfo& objVI, string& strMessage){
	strMessage += to_string(objVI.dGPSX) += ',';
	strMessage += to_string(objVI.dGPSY) += ',';
	strMessage += to_string(objVI.dSteering) += ',';
	strMessage += to_string(objVI.nVelo) += '/';
}
void MakeDataMessage(vector<SObjectHD>& objObjectHD, string& strMessage, int nFrameNumber){
	//strMessage += "ANS";
	//strMessage += '/';
	strMessage += to_string(nFrameNumber) += ' ';
	strMessage += to_string(objObjectHD.size()) += ' ';
	for (int i = 0; i < objObjectHD.size(); i++){
		strMessage += to_string(objObjectHD[i].dZ) += ' ';
		strMessage += to_string(objObjectHD[i].dX) += ' ';
		strMessage += to_string(objObjectHD[i].dWidth) += ' ';
		strMessage += to_string(objObjectHD[i].nClass) += ' ';
	}
	strMessage += '=';
}


int main()
{
	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN recvAddr;
	WSABUF dataBuf;
	WSABUF temp_Buf;
	char *client_ID, *client_State, *client_DataType, *client_Data;
	string C_ID, C_State, C_DataType, C_data;
	char message[1024] = "";
	char ID1[8] = { 0, };
	char State[1024] = "";
	char data[10000] = "";
	char REG_NUM[1024] = "0691/";
	char REQ[1024] = "0691/";
	int sendBytes = 0;
	int recvBytes = 0;
	int flags = 0;
	//itoa(ID, ID1, 10);
	WSAEVENT event;
	WSAOVERLAPPED overlapped;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) // Load Winsock 2.2 DLL
		ErrorHandling("WSAStartup() error!");

	hSocket = WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSocket == INVALID_SOCKET)
		ErrorHandling("socket() error");

	memset(&recvAddr, 0, sizeof(recvAddr));
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = inet_addr("10.10.100.89");
	recvAddr.sin_port = htons(atoi("2738"));
	if (connect(hSocket, (SOCKADDR*)&recvAddr, sizeof(recvAddr)) == SOCKET_ERROR)
		ErrorHandling("connect() error!");

	cout << "**** Client ****" << endl;
	//cout << "**** ID : " << "0691" << " ****" << endl;
	// 구조체에 이벤트핸들 삽입해서 전달
	event = WSACreateEvent();
	memset(&overlapped, 0, sizeof(overlapped));
	overlapped.hEvent = event;

	strcat(REG_NUM, "000/REG_NUM/0691");

	dataBuf.len = strlen(REG_NUM);
	dataBuf.buf = REG_NUM;
	if (WSASend(hSocket, &dataBuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
			ErrorHandling("WSASend() error");
	}

	Mat imgFinalDisplay(objParam.objCamParam.m_sizeSrc.height, objParam.objCamParam.m_sizeSrc.width / 2, CV_8UC3);
	Mat imgStixel;
	char chFPS[30] = { 0, };

	char chLeftName[200], chRightName[200];
	int cntImageIndex = 0;

	bool flgOK;
	CStereoVisionForADAS objSV(objParam);
	CIntegrationVD objVD(flgOK);

	vector<SObjectHD> objObjectHD;

	namedWindow("result", 0);
	namedWindow("topview", 0);

	int nVelo = 10;

	fstream fs;
	fs.open("Front_car.txt", ios::out);

	while (1){
		Mat imgTopView(600, 500, CV_8UC3);

		string strMessage = "";
		char chCarNumber[] = "0691";

		objObjectHD.clear();
		sprintf(chLeftName, "C:/Users/cvlab/Desktop/DB/2011_09_26_drive_0056_sync/2011_09_26/2011_09_26_drive_0056_sync/image_02/data/%d.png", cntImageIndex);
		sprintf(chRightName, "C:/Users/cvlab/Desktop/DB/2011_09_26_drive_0056_sync/2011_09_26/2011_09_26_drive_0056_sync/image_03/data/%d.png", cntImageIndex);
		
		if (cntImageIndex > 293) cntImageIndex = 0;

		Mat imgLeftColor = imread(chLeftName, 1);
		if (imgLeftColor.empty()) return 0;
		Mat imgRightColor = imread(chRightName, 1);
		Mat imgResult = imgLeftColor.clone();
		if (imgLeftColor.rows != 375 || imgLeftColor.cols != 1242) {
			continue;
		}
		int64 t = getTickCount();
		objSV.Objectness(imgLeftColor, imgRightColor);

		vector<Object_t> vecobjCarHypothesis = objSV.m_vecobjBB;
		objVD.RunOnce(imgLeftColor, vecobjCarHypothesis);

		cvtStixelObj2HDobj(vecobjCarHypothesis, objObjectHD);

		//strMessage += "0691";
		//strMessage += '/';
		
		SVehicleInfo objVI;
		objVI.dGPSX = 0;
		objVI.dGPSY = 0;
		objVI.dSteering = 10;
		objVI.nVelo = 60;

		//MakeStateMessage(objVI, strMessage);
		MakeDataMessage(objObjectHD, strMessage, cntImageIndex);
		//cout << strMessage.length() << endl;
		//cout << strMessage << endl;

		fs << strMessage << endl;

		flags = 0;
		//cout << " Send data : (exit : q)" << endl;
		//scanf("%s", message);
		if (!strcmp(message, "q")) break;
		dataBuf.len = 1000;
		dataBuf.buf = message;

		//WSARecv(hSocket, &dataBuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL);
		//string temp;
		//temp = dataBuf.buf;
		//if (temp == "RES")
		//{
		//	char ANS_Env[3000] = "";
		//	strcat(ANS_Env, "0691"); strcat(ANS_Env, "/0,0,0/ANS/");
		//	strcat(ANS_Env, strMessage.c_str());
		//	temp_Buf.buf = ANS_Env;
		//	temp_Buf.len = strlen(temp_Buf.buf);
		//	if (WSASend(hSocket, &temp_Buf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
		//	{
		//		if (WSAGetLastError() != WSA_IO_PENDING)
		//			ErrorHandling("WSASend() error");
		//	}
		//	else{
		//		//cout << temp_Buf.buf << endl;
		//		//cout << temp_Buf.len << endl;
		//		
		//	}
		//}

		float fTime = (getTickCount() - t) * 1000 / getTickFrequency();
		sprintf(chFPS, "fps : %.2f", 1000 / fTime);

		objSV.Display(imgResult, imgStixel);
		putText(imgResult, chFPS, Point(5, 12), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);
		TopView(imgTopView, objObjectHD, objSV.m_imgGround, nVelo);

		for (int i = 0; i < vecobjCarHypothesis.size(); i++){
			if (vecobjCarHypothesis[i].nClass == Car){
				rectangle(imgResult, vecobjCarHypothesis[i].rectBB, Scalar(0, 255, 0), 4);
				rectangle(imgStixel, vecobjCarHypothesis[i].rectBB, Scalar(0, 255, 0), 4);
			}
		}

		imgFinalDisplay;
		resize(imgResult, imgFinalDisplay(Rect(0, 0, imgFinalDisplay.cols, imgFinalDisplay.rows / 2)), Size(imgFinalDisplay.cols, imgFinalDisplay.rows / 2));
		resize(imgStixel, imgFinalDisplay(Rect(0, imgFinalDisplay.rows/2, imgFinalDisplay.cols, imgFinalDisplay.rows / 2)), Size(imgFinalDisplay.cols, imgFinalDisplay.rows / 2));

		imshow("result", imgFinalDisplay);
		//imshow("stixel", imgStixel);
		char chVelo[20];
		sprintf(chVelo, "speed:%dkm/h", nVelo);
		putText(imgTopView, chVelo, Point(10, 550), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 2);
		imshow("topview", imgTopView);
		
		char chKey = waitKey(1);
		if (chKey == 27) return 0;
		else if (chKey == ' ') waitKey(0);
		else if (chKey == '+') nVelo < 120 ? nVelo += 10 : nVelo = 110;
		else if (chKey == '-') nVelo > 0 ? nVelo -= 10 : nVelo = 0;
		cntImageIndex++;
	}
	closesocket(hSocket);
	WSACleanup();
	return 0;
}
void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}
