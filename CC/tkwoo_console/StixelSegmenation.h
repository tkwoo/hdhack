/**
	@file StixelSegmentation.h
	@date 2106/03/04
	@author 우태강(tkwoo@vision.inha.ac.kr) Inha Univ.
	@brief stixel Segmentation을 위한 헤더파일
*/
#pragma once

// #include <opencv2\opencv.hpp>
#include <iostream>
#include <map>

#include "../include/DefStruct.h"
#include "StixelEstimation.h"

using namespace std;
using namespace cv;

typedef enum Seg_error { GOOD, SIZ_ERR } SEG_ERROR;


//struct boundingbox_t
//{
//	Rect bb;			///< boundbox
//	double dZ;			///< distance(meter)
//	vector<stixel_t> objStixels;
//	boundingbox_t(Rect rect, double dz)
//	{
//		bb = rect; dZ = dz;
//		objStixels.clear();
//	}
//	boundingbox_t(Rect rect, stixel_t objStixel)
//	{
//		objStixels.clear();
//		bb = rect; dZ = objStixel.dZ;
//		objStixels.push_back(objStixel);
//	}
//};



/**
	@class CStixelSegmentation
	@date 2106/03/04
	@author 우태강(tkwoo@vision.inha.ac.kr) Inha Univ.
	@brief stixel Segmentation을 위한 class
*/
class CStixelSegmentation{
private:
	//----------------input--------------------
	vector<stixel_t> m_vecobjStixels;

	//----------------param--------------------
	StereoCamParam_t m_objStereoParam;

	//-------------member image----------------
	Mat m_imgDebug;
	
	//---------------function------------------
	//============processing part==============

	//old version
	//SEG_ERROR StixelZClustering(vector<stixel_t>& objStixels, vector<boundingbox_t>& objBBcandidate); ///< Z 방향 Distance 기반 분류, Stixel -> BB candidate
	//SEG_ERROR StixelXClustering(vector<boundingbox_t>& objBBinput, vector<boundingbox_t>& objBBOutput); ///< X 방향 Distance 기반 분류, BB candidate -> BB result
	//SEG_ERROR StixelBBboxOptimization(vector<boundingbox_t>& objBBinput, vector<boundingbox_t>& objBBOutput); ///< bounding box 최적화

	SEG_ERROR StixelZClustering(vector<stixel_t>& objStixels, vector<Object_t>& objBBcandidate); ///< Z 방향 Distance 기반 분류, Stixel -> BB candidate
	SEG_ERROR StixelXClustering(vector<Object_t>& objBBinput, vector<Object_t>& objBBOutput); ///< X 방향 Distance 기반 분류, BB candidate -> BB result
	SEG_ERROR StixelBBboxOptimization(vector<Object_t>& objBBinput, vector<Object_t>& objBBOutput); ///< bounding box 최적화

public:
	//----------------output-------------------
	vector<Object_t> m_vecobjBB;

	//---------------function------------------
	CStixelSegmentation(StereoCamParam_t objStereoParam);
	void SetDebugImg(Mat imgTemp);

	//wrapping function
	SEG_ERROR SegmentStixel(vector<stixel_t>& objStixels); ///< Stixel 분류작업
	
	
};
