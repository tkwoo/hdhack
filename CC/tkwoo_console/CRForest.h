/* 
// Author: Juergen Gall, BIWI, ETH Zurich
// Email: gall@vision.ee.ethz.ch
*/

#pragma once

#include "CRTree.h"
#include "CRConfig.h"

#include <vector>
using namespace std;

class CRForest {
private:
	int nTreeSize;
	const char* Treefilename;

public:
	// Constructors
	CRForest(const CRConfig& crCF)
		: nTreeSize(crCF.nTrees)
	{
		Treefilename = crCF.strTreepath.c_str();
		vTrees.resize(nTreeSize);
		loadForest(Treefilename);
	}

	~CRForest() {
		for(vector<CRTree*>::iterator it = vTrees.begin(); it != vTrees.end(); ++it) delete *it;
		vTrees.clear();
	}

	unsigned int GetNumCenter() const { return vTrees[0]->GetNumCenter(); }
	// Regression 
	void regression(std::vector<const LeafNode*>& result, uchar ** ptFCh, int stepImg) const;

	// IO functions
	void loadForest(const char* filename) ;

	// Trees
	vector<CRTree*> vTrees;
};


inline void CRForest::regression(vector<const LeafNode*>& result, uchar ** ptFCh, int stepImg) const {
	result.resize(vTrees.size());
	for (int i = 0; i < (int)vTrees.size(); ++i) {	//vTree size: 15
		result[i] = vTrees[i]->regression(ptFCh, stepImg);	//ptFCh: 1 row 
	}
}

inline void CRForest::loadForest(const char* filename) {
	char buffer[200];
	//cout << "vTrees.size() " << vTrees.size() << endl;
	for(unsigned int i=0; i<vTrees.size(); ++i) {
		sprintf(buffer,"%s%03d.txt",filename,i);
		vTrees[i] = new CRTree(buffer);
	}
}



