#include <opencv2\opencv.hpp>
#include <iostream>
#include <fstream>
#include "StereoVisionForADAS.h"

using namespace std;
using namespace cv;

StereoCamParam_t objParam = CStereoVisionForADAS::InitStereoParam(KITTI);

Mat imgTopView(600, 500, CV_8UC3);

void TopView(Mat& imgTopView, vector<SObjectHD>& objDetectedObject){
	Mat imgCarTopView = imread("./image/car_topview.png", 1);
	imgTopView.setTo(255);
	Mat imgPartCenterCar = imgTopView(Rect(235, 495, 50, 50));
	resize(imgCarTopView, imgPartCenterCar, imgPartCenterCar.size());
	for (int i = 100; i <= 500; i += 100) line(imgTopView, Point(0, i), Point(imgTopView.cols, i), Scalar(100, 100, 100));

	for (int i = 0; i < objDetectedObject.size(); i++){
		Point ptTemp(250 - 10*objDetectedObject[i].dX, 500 - 10*objDetectedObject[i].dZ);
		line(imgTopView, ptTemp, ptTemp + Point(10*objDetectedObject[i].dWidth, 0), Scalar(0, 0, 255), 3);
	}

}
void cvtStixelObj2HDobj(vector<Object_t>& objDetectedObject, vector<SObjectHD>& objObjectHD){
	for (int i = 0; i < objDetectedObject.size(); i++){
		SObjectHD objTemp;
		objTemp.dZ = objDetectedObject[i].dZ;
		objTemp.dX = (objParam.objCamParam.m_sizeSrc.width/2 - objDetectedObject[i].rectBB.x)*objTemp.dZ / objParam.objCamParam.m_dFocalLength;
		objTemp.dWidth = objDetectedObject[i].rectBB.width*objTemp.dZ / objParam.objCamParam.m_dFocalLength;
		objTemp.nClass = objDetectedObject[i].nClass;
		objObjectHD.push_back(objTemp);
	}
}

int main()
{
	Mat imgStixel;
	char chFPS[30] = { 0, };

	char chLeftName[100], chRightName[100];
	int cntImageIndex = 0;

	CStereoVisionForADAS objSV(objParam);

	while (1){
		sprintf(chLeftName, "Left150.png");
		sprintf(chRightName, "Right150.png");
		cntImageIndex++;

		Mat imgLeftColor = imread(chLeftName, 1);
		if (imgLeftColor.empty()) return 0;
		Mat imgRightColor = imread(chRightName, 1);
		Mat imgResult = imgLeftColor.clone();
		if (imgLeftColor.rows != 375 || imgLeftColor.cols != 1242) {
			continue;
		}
		int64 t = getTickCount();
		objSV.Objectness(imgLeftColor, imgRightColor);

		float fTime = (getTickCount() - t) * 1000 / getTickFrequency();
		sprintf(chFPS, "fps : %.2f", 1000 / fTime);

		objSV.Display(imgResult, imgStixel);
		putText(imgResult, chFPS, Point(5, 12), FONT_HERSHEY_PLAIN, 1, Scalar(0, 255, 0), 1);
		imshow("result", imgResult);
		imshow("stixel", imgStixel);

		vector<SObjectHD> objObjectHD;
		cvtStixelObj2HDobj(objSV.m_vecobjBB, objObjectHD);
		TopView(imgTopView, objObjectHD);

		imshow("topview", imgTopView);

		if (waitKey(1) == 27) return 0;
	}

	return 0;
}