#include "CRForestDetector.h"

Mat CRForestDetector::MinFilter2(Mat& imgSrc, int nX, int nY){
	int nWidth = imgSrc.cols;
	int nHeight = imgSrc.rows;

	Mat tmp = Mat(imgSrc.size(), CV_8UC1, Scalar(0));

	int nMin;

	for (int j = 0; j < nHeight; j++)
		for (int i = 0; i < nWidth; i++){
		nMin = imgSrc.at<uchar>(j, i);
		for (int n = j - nY; n < j + nY; n++)
			for (int m = i - nX; m < i + nX; m++)
			{
			if (n > -1 && n < nHeight && m > -1 && m < nWidth)
			{
				if (nMin > imgSrc.at<uchar>(n, m))
					nMin = imgSrc.at<uchar>(n, m);
			}
			}
		tmp.at<uchar>(j, i) = (uchar)nMin;
		}

	return tmp;

}

Mat CRForestDetector::MaxFilter2(Mat& imgSrc, int nX, int nY){
	int nWidth = imgSrc.cols;
	int nHeight = imgSrc.rows;

	Mat tmp = Mat(imgSrc.size(), CV_8UC1, Scalar(0));

	int nMax;

	for (int j = 0; j < nHeight; j++)
		for (int i = 0; i < nWidth; i++){
		nMax = imgSrc.at<uchar>(j, i);
		for (int n = j - nY; n < j + nY; n++)
			for (int m = i - nX; m < i + nX; m++)
			{
			if (n > -1 && n < nHeight && m > -1 && m < nWidth)
			{
				if (nMax < imgSrc.at<uchar>(n, m))
					nMax = imgSrc.at<uchar>(n, m);
			}
			}
		tmp.at<uchar>(j, i) = (uchar)nMax;
		}

	return tmp;

}

void CRForestDetector::ProjectImage(Mat& img, vector<float>& vecXProject, vector<float>& vecYProject){
	int nWidth, nHeight;
	nWidth = img.cols;
	nHeight = img.rows;
	vecXProject.resize(nWidth);
	vecYProject.resize(nHeight);
	for (int i = 0; i < nHeight; i++){
		for (int j = 0; j < nWidth; j++){
			vecXProject[j] += (unsigned int)img.at<int>(i, j);
		}
	}
}

void CRForestDetector::FindMaximum(Mat& imgHough, float nThreshold, float fScale, vector<sPointInfo>& vecPoint){
	double dMax, dTempMax;
	Point pIdx;
	vector<int> vecXLocation, vecYLocation;
	vector<double> vecMaxIntensity;
	vector<float> vecXProjection, vecYProjection;
	Mat mHist;
	int nHistSize = 256;
	Point pt1, pt2;
	int nPtStart = 0;

	vecXProjection.clear(); vecYProjection.clear();

	//prject image to x and y direction;
	ProjectImage(imgHough, vecXProjection, vecYProjection);
	Mat mTemp(vecXProjection);

	int nRow = mTemp.rows;
	Mat mResult = Mat::zeros(nRow, 1, CV_32FC1);

	//get the maximum value of mTemp
	minMaxLoc(mTemp, 0, &dMax);
	dTempMax = dMax*0.5;

	//calculate the derivative of image
	for (int i = 1; i < nRow; i++)
		mResult.ptr<float>(i)[0] = mTemp.ptr<float>(i)[0] - mTemp.ptr<float>(i - 1)[0];

	//find the index of potential maximal value in x direction
	for (int i = 1; i < nRow; i++){
		if (mResult.ptr<float>(i - 1)[0] > 0 && mResult.ptr<float>(i)[0]<0 && mTemp.ptr<float>(i)[0] >dTempMax)
			vecXLocation.push_back(i);
	}

	//find index of potential maximal value in y direction and its intensity
	for (unsigned int i = 0; i < vecXLocation.size(); i++){
		mTemp = imgHough.col(vecXLocation[i]);
		minMaxLoc(mTemp, 0, &dMax, 0, &pIdx);
		vecYLocation.push_back(pIdx.y);
		vecMaxIntensity.push_back(dMax);
	}

	for (unsigned int i = 0; i < vecXLocation.size(); i++){
		if (vecMaxIntensity[i] > nThreshold){	//check 조건추가
			//cout << "intensity > 1" << endl;
			sPointInfo pointInfo;
			pointInfo.nX = (int)(vecXLocation[i] / (float)fScale);
			pointInfo.nY = vecYLocation[i] / (float)fScale;
			pointInfo.nIntensity = vecMaxIntensity[i] / (float)fScale;
			pointInfo.fScale = fScale;
			vecPoint.push_back(pointInfo);
		}
	}
}


void CRForestDetector::FindMaximum2(Mat& imgHough, float fScale, vector<sPointInfo>& vecPoint){
	double dMax, dTempMax;
	Point pIdx;
	vector<int> vecXLocation, vecYLocation;
	vector<double> vecMaxIntensity;
	vector<float> vecXProjection, vecYProjection;
	Mat mHist;
	int nHistSize = 256;
	Point pt1, pt2;
	int nPtStart = 0;

	vecXProjection.clear(); vecYProjection.clear();

	//prject image to x and y direction;
	ProjectImage(imgHough, vecXProjection, vecYProjection);
	Mat mTemp(vecXProjection);

	int nRow = mTemp.rows;
	Mat mResult = Mat::zeros(nRow, 1, CV_32FC1);

	//get the maximum value of mTemp
	minMaxLoc(mTemp, 0, &dMax);
	dTempMax = dMax*0.5;

	//calculate the derivative of image
	for (int i = 1; i < nRow; i++)
		mResult.ptr<float>(i)[0] = mTemp.ptr<float>(i)[0] - mTemp.ptr<float>(i - 1)[0];

	//find the index of potential maximal value in x direction
	for (int i = 1; i < nRow; i++){
		if (mResult.ptr<float>(i - 1)[0] > 0 && mResult.ptr<float>(i)[0]<0 && mTemp.ptr<float>(i)[0] >dTempMax)
			vecXLocation.push_back(i);
	}

	//find index of potential maximal value in y direction and its intensity
	for (unsigned int i = 0; i < vecXLocation.size(); i++){
		mTemp = imgHough.col(vecXLocation[i]);
		minMaxLoc(mTemp, 0, &dMax, 0, &pIdx);
		vecYLocation.push_back(pIdx.y);
		vecMaxIntensity.push_back(dMax);
	}

	// 정규화
	Mat mMean, mStddev;
	sMeanStddev vecMeanStdDev;
	double dMean, dStddevValue;
	meanStdDev(imgHough, mMean, mStddev);
	dMean = mMean.at<double>(0, 0);
	dStddevValue = mStddev.at<double>(0, 0);
	double dNormMaxIntensity = (dMax - dMean) / (double)dStddevValue;
	printf("dMax %f\n", dMax);
	printf("dMean %f\n", dMean);

	printf("dNormMaxIntensity %f\n", dNormMaxIntensity);
//	exit(-1);

	for (unsigned int i = 0; i < vecXLocation.size(); i++){
		double dNormIntensity = (vecMaxIntensity[i] - dMean) / dStddevValue;
		if (dNormIntensity > dNormMaxIntensity*0.8){	//check 조건추가
			printf("dNormIntensity %f\n", dNormIntensity);
			waitKey(3000000);
		//if (vecMaxIntensity[i] > nThreshold){	//check 조건추가
			//cout << "intensity > 1" << endl;
			sPointInfo pointInfo;
			pointInfo.nX = (int)(vecXLocation[i] / (float)fScale);
			pointInfo.nY = vecYLocation[i] / (float)fScale;
			pointInfo.nIntensity = vecMaxIntensity[i] / (float)fScale;
			pointInfo.fScale = fScale;
			vecPoint.push_back(pointInfo);
		}
	}
	//cout <<  "vecPoint size" << vecPoint.size() << endl;

}

bool CRForestDetector::LoadKITTIGT(const char* filename)
{
	char buff[400];
	ifstream in(filename);
	if (!in.is_open()) return false;
	vector<sKITTI_GT> vec_sK_GT;

	string strFrameNum ="";
	int nVehicleNum = 0;
	float fObservationAngle = 0;
	int nOcclusion = 0;
	vector<int> vecLoc;
	Rect rectGT;

	cout << "start loading KITTI GT" << endl;

	while (in.good()){
		if (!in.good())break;
		char* strTemp;
		int nTmp;

		vec_sK_GT.clear();

		// frame 단위 처리
		in.getline(buff, 400);
		if (buff == NULL) break;
		strTemp = buff;

		char *p = strtok(strTemp, "\t");
		//strFrameNum = p;
		//printf("%s\n", p);
		p = strtok(NULL, "\t");

		std::istringstream iss(p);
		iss >> nVehicleNum;

		vec_sK_GT.resize(nVehicleNum);
		for (int i = 0; i < nVehicleNum; i++){
			//cout << "V idx: "<<i+1 << endl;

			in.getline(buff, 400);
			strTemp = buff;
			char *objType = strtok(strTemp, "\t");
			//printf("%s\n", objType);
			
			p = strtok(NULL, "\t");
			fObservationAngle = stoi(p);
			//printf("%f\n", fObservationAngle);

			in.getline(buff, 400);
			//strTemp = buff;
			//p = strtok(strTemp, "\t");
			nOcclusion = stoi(strTemp);

			vecLoc.clear();
			in.getline(buff, 400);
			strTemp = buff;
			char *VehicleLoc = strtok(strTemp, "\t");
	
			while (VehicleLoc){
				vecLoc.push_back(stoi(VehicleLoc));
				VehicleLoc = strtok(NULL, "\t");
			}
			
			if (vecLoc[0] > vecLoc[2]){
				nTmp = vecLoc[0];
				vecLoc[0] = vecLoc[2];
				vecLoc[2] = nTmp;
			}
			if (vecLoc[1] > vecLoc[3]){
				nTmp = vecLoc[1];
				vecLoc[1] = vecLoc[3];
				vecLoc[3] = nTmp;
			}

			rectGT = Rect(vecLoc[0], vecLoc[1], vecLoc[2] - vecLoc[0], vecLoc[3] - vecLoc[1]);
			vec_sK_GT[i].type = objType;
			vec_sK_GT[i].fAlpha = fObservationAngle;
			vec_sK_GT[i].nOcclusion = nOcclusion;
			vec_sK_GT[i].rectBB = rectGT;

		}
		vec_sKITTI_GT.push_back(vec_sK_GT);
		if (!in.good())break;
	}

	cout << "finished loading KITTI GT" << endl;

}

bool CRForestDetector::LoadCVLABGT(const char* filename){
	char buffer[1024];
	ifstream in(filename);
	if (!in.is_open()) return false;
	string strTemp;
	int nFrameNum, nVehicleNum;
	//int nX=0, nY=0, nWidth=0, nHeight=0;
	vector<int> vecLoc;
	Rect rectGT;
	vector<Rect> vecRectGT;


	while (in.good()){
		if (!in.good())break;
		char* strTemp;
		vecRectGT.clear();

		/////frame 단위 처리
		in.getline(buffer, 400);
		if (buffer == NULL) break;
		strTemp = buffer;

		char *p = strtok(strTemp, ",");
		nFrameNum = stoi(p);
		//cout <<"frame:	"<< nFrameNum << endl; 
		p = strtok(NULL, ",");
		nVehicleNum = stoi(p);

		for (int i = 0; i < nVehicleNum; i++){
			vecLoc.clear();
			in.getline(buffer, 400);
			strTemp = buffer;
			char *VehicleLoc = strtok(strTemp, ",");
			while (VehicleLoc){
				vecLoc.push_back(stoi(VehicleLoc));
				VehicleLoc = strtok(NULL, ",");
			}
			rectGT = Rect(vecLoc[0], vecLoc[1], vecLoc[2], vecLoc[3]);
			vecRectGT.push_back(rectGT);
		}
		vecGT.push_back(vecRectGT);
		if (!in.good())break;
	}
	//cout << "loaded GT" << endl;

	//for (int i = 0; i < vecRectGT.size(); i++){
	//	rectangle(mTotalImg, vecRectGT[i], Scalar(0, 255, 0), 2);
	//}
}

void CRForestDetector::DrawKITTIGT(int nIdx, Mat& imgDisp)
{
	char imgName[80];

	vector<sKITTI_GT> vec_sKGT;
	vec_sKGT = vec_sKITTI_GT[nIdx];

	for (int i = 0; i < vec_sKGT.size(); i++){
		
#if STORE_IMAGME_CROP
		if (vec_sKGT[i].rectBB.width >= 30 && vec_sKGT[i].rectBB.height >=30 
			&& vec_sKGT[i].rectBB.tl().x > 0 && vec_sKGT[i].rectBB.tl().y > 0 && vec_sKGT[i].rectBB.br().x < imgSrc.cols && vec_sKGT[i].rectBB.br().y < imgSrc.rows){
			Mat imgCrop = imgDisp(vec_sKGT[i].rectBB);

			// 이 부분은 지울것
#if EVALUATE
			sprintf(imgName, "KITTI_GT_Neg/%d_%d.png", nIdx, i);
			imwrite(imgName, imgCrop);
#else 
			if (vec_sKGT[i].nOcclusion == 0){
				sprintf(imgName, "KITTI_GT_rear/%d_%d.png", nIdx, i);
				imwrite(imgName, imgCrop);
			}
			else if (vec_sKGT[i].nOcclusion==1){
				sprintf(imgName, "KITTI_GT_rear_occlusion/%d_%d.png", nIdx, i);
				imwrite(imgName, imgCrop);
		}

#endif
		}
#else 
		rectangle(imgDisp, vec_sKGT[i].rectBB, CV_RGB(255, 255, 0), 2);
#endif
	}
}

void CRForestDetector::DrawCVLABGT(int nIndx, Mat& mInput){
	//nIndx = nIndx - 1;
	Mat cropImg;
	char imgName[1024];
	int nGTCnt = 0;
	Rect rectOverlapLeft, rectOverlapRight;

	for (int i = 0; i < nIndx - 1; i++)
		nGTCnt += vecGT[i].size();

	Rect rectCIROI_right = Rect(840,285,436,245);
	Rect rectCIROI_left = Rect(12,285, 436, 245);

	//cout << "nGTCnt "<<nGTCnt << endl;

	for (int i = 0; i < vecGT[nIndx].size(); i++){
		
		rectOverlapRight = vecGT[nIndx][i] & rectCIROI_right;
		rectOverlapLeft = vecGT[nIndx][i] & rectCIROI_left;

		if ((rectOverlapRight.width>0 && rectOverlapRight.height>0) || (rectOverlapLeft.width>0 && rectOverlapLeft.height>0)){
			if (vecGT[nIndx][i].x < 0)  vecGT[nIndx][i].x = 0;
			else if (vecGT[nIndx][i].br().x > mInput.cols)  vecGT[nIndx][i].width = mInput.cols - vecGT[nIndx][i].x - 1;
			if (vecGT[nIndx][i].y < 0)  vecGT[nIndx][i].y = 0;
			else if (vecGT[nIndx][i].br().y > mInput.rows)  vecGT[nIndx][i].height = mInput.rows - vecGT[nIndx][i].y - 1;

			cropImg = mInput(vecGT[nIndx][i]);
			sprintf(imgName, "CVLAB_GT/negative/4/%d.png", nGTCnt + i);
			imwrite(imgName, cropImg);
		}
	}

	for (int k = 0; k < vecGT[nIndx].size();k++){
		rectangle(mInput, vecGT[nIndx][k], Scalar(0, 255, 255), 3);
	}

}

void CRForestDetector::InitializeOutput(sOutput& out){
	out.TP = out.TN = out.FP = out.FN = out.GT = 0;
}


void CRForestDetector::EvaluatePerformance(vector<Rect>& vecDetected, int nIndx, sOutput& out){
	int nDetectionCnt = vecDetected.size();
	int nGroundTruthCnt = vec_sKITTI_GT[nIndx].size();
	int nTruePositive = 0;
	int nFalsePositive = 0;
	int nFalseNegative = 0;
	int Union, intersect = 0;
	Rect  rectIntersect;
	int nUnion;
	float fOverlapRatio = 0;

	//int nObjID = 0;
	bool bIsIn = 0;

#if STORE_IMAGE_NEG_CROP
	vector<int> vec_TPIdx;
	vector<vector<int>> vec_vecTPIdx;
#endif


	//cout << "detect 개수 " << vecDetected.size() << endl;

	for (int i = 0; i < nGroundTruthCnt; i++){
		Rect rectGT = vec_sKITTI_GT[nIndx][i].rectBB;
	
		for (int j = 0; j < nDetectionCnt; j++){
			Rect rectDetection = vecDetected[j];

			//Calculate Overlap Ratio
			rectIntersect = rectGT&rectDetection;
			nUnion = rectGT.area() + rectDetection.area() - rectIntersect.area();
			fOverlapRatio = rectIntersect.area() / (float)nUnion;
			if (fOverlapRatio > 0.5){
				nTruePositive++;		
#if STORE_IMAGE_NEG_CROP
				vec_TPIdx.push_back(j);
#endif
			}
		}
		//vec_vecTPIdx.push_back(vec_TPIdx);
	}

#if STORE_IMAGE_NEG_CROP
	int nTPIdx = 0;
	bool bIsTP = false;
	bool bIsCrop = true;
	char strBuff[50];

	for (int d = 0; d < vecDetected.size(); d++){
		for (int c = 0; c < vec_TPIdx.size(); c++){
			nTPIdx = vec_TPIdx[c];
			if (d == nTPIdx){ 
				bIsTP = true;
				break;
	/*			vecDetected.erase(vecDetected.begin()+d);
				d--;*/
			}
			else {
				bIsTP = false;
			}
//			bIsCrop = bIsTP&bIsCrop;
		}
		if (!bIsTP){
			if (vecDetected[d].x > 0 && vecDetected[d].y>0 && vecDetected[d].br().x < imgSrc.cols&&vecDetected[d].br().y < imgSrc.rows
				&& vecDetected[d].width>0 && vecDetected[d].height>0){
				Mat imgCrop = imgSrc(vecDetected[d]);
				sprintf(strBuff, "neg_crop/%d_%d.png", nFrameIdx, d);
				imwrite(strBuff, imgCrop);

			}
		}
	}

#endif

	nFalsePositive = nDetectionCnt - nTruePositive;			//False alarm
	nFalseNegative = nGroundTruthCnt - nTruePositive;	//Missing	
	out.GT += nGroundTruthCnt;
	out.TP += nTruePositive;
	out.FP += nFalsePositive;
	out.FN += nFalseNegative;

	//if (nFalsePositive > 0 || nGroundTruthCnt > nTruePositive){
	printf("[frame %d, (%d)]\n", nIndx, nGroundTruthCnt);
	printf("TP : %d\n", nTruePositive);
	printf("FP : %d\n\n", nFalsePositive);
	//}

}

void CRForestDetector::PrintEvaluateResult(sOutput& out){
	float fCorrect = vecDetectObjIdList.size() / (float)vecGTObjIdList.size();
	fCorrect = fCorrect * 100;

	printf("Detected tracklets : %d\n", vecDetectObjIdList.size());
	printf("GT tracklets : %d\n", vecGTObjIdList.size());
	printf("Correct : %f\n\n", fCorrect);

	printf("GT : %d\n", out.GT);
	printf("TP : %d\n", out.TP);
	printf("FP : %d\n", out.FP);
	printf("FN : %d\n", out.FN);
	printf("Recall    : %.3f\n", (float)out.TP / (out.TP + out.FN));
	printf("Precision : %.3f\n", (float)out.TP / (out.TP + out.FP));
}


bool CRForestDetector::LoadKITTIObjData(const string& strTestDataPath, int& nDataIdx, Mat& imgInput)
{
	char strImgName[20];
	sprintf(strImgName,"%06d.png", nDataIdx);
	string strDataPath = strTestDataPath + "/" + strImgName;
	imgInput = imread(strDataPath);
	//imshow("input", imgInput);
	//cvWaitKey(0);

	if (!imgInput.data) {
		//cout << "ERROR: Test data not loaded" << endl;
		return false;
	}
}

void CRForestDetector::FilterGT(int nIdx)
{
	vector<sKITTI_GT> vec_sKGT;
	vec_sKGT = vec_sKITTI_GT[nIdx];
	cout << "vec_sKITTI_GT[nIdx] " << vec_sKITTI_GT[nIdx].size() << endl;


	for (int i = 0; i < vec_sKGT.size(); i++){
		
		int nRound = Rounding((float)vec_sKGT[i].fAlpha, 2);
		
		// 1st, ROI가 이미지를 벗어낫는지 확인
		// 2nd, Oversation angle 확인
		// 3nd, Occlusion 확인

		if (vec_sKGT[i].rectBB.tl().x <= 0 || vec_sKGT[i].rectBB.tl().y <= 0 || vec_sKGT[i].rectBB.br().x >= imgSrc.cols || vec_sKGT[i].rectBB.br().y >= imgSrc.rows  
			|| nRound != -1
			|| vec_sKGT[i].nOcclusion > 1
			|| vec_sKGT[i].rectBB.width <20 || vec_sKGT[i].rectBB.height<20 ){
			vec_sKGT.erase(vec_sKGT.begin()+i);
			i--;
		}
	}

#if STORE_GTnDT_TXT
	for (int i = 0; i < vec_sKGT.size(); i++){
		fprintf(fGT, "Car %d %f %d %d %d %d -1 -1 -1 -1 -1 -1 -1\n",
			vec_sKGT[i].nOcclusion, vec_sKGT[i].fAlpha, vec_sKGT[i].rectBB.x, vec_sKGT[i].rectBB.y, vec_sKGT[i].rectBB.br().x, vec_sKGT[i].rectBB.br().y);
	}
#endif

	vec_sKITTI_GT[nIdx] = vec_sKGT;
	cout << "vec_sKITTI_GT[nIdx] " << vec_sKITTI_GT[nIdx].size() << endl;
}

