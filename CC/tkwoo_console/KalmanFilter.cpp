#include "KalmanFilter.h"

void CKalman::DrawCross(Mat img, Point center, Scalar color, int d)
{
	line(img, Point(center.x - d, center.y - d), Point(center.x + d, center.y + d), color, 2, CV_AA, 0);
	line(img, Point(center.x + d, center.y - d), Point(center.x - d, center.y + d), color, 2, CV_AA, 0);
}

void CKalman::SetKalmanParameter(KalmanFilter& KF, Mat_<float>& measurement)
{

	//// -------------------------------------------------------- Initialise Kalman parameters 

	float dt = 0.1f;
	KF.transitionMatrix = *(Mat_<float>(8, 8) <<
		1.0f, 0.0f, dt, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, dt, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);  //A
	//KF.measurementMatrix = *(Mat_<float>(2, 4) << 1,0,0,0,   0,1,0,0);

	//transitionMatirx : 전이행렬

	//단위행렬로 초기화

	//측정행렬
	setIdentity(KF.measurementMatrix);
	//프로세스 잡음 공분산, 클수록 수정값이 많이 변함
	setIdentity(KF.processNoiseCov, Scalar::all(1e-1)); //1e-4, 따라오는 속도, 클수록 빠름 
	//측정 잡음 공분산, 작을수록 수정값이 많이 변함
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-11));	//따라오는 속도, 작을수록 빠름 
	//사후 에러 공분산
	setIdentity(KF.errorCovPost, Scalar::all(.1));

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


}

int CKalman::DoKalmanFiltering(StructKalman& MultiKF, Rect& rec, Mat& imgROImask)
{
	if (MultiKF.ptCenter.x == 0 && MultiKF.ptCenter.y == 0)
	{
		MultiKF.ptCenter.x = MultiKF.ptEstimate.x;
		MultiKF.ptCenter.y = MultiKF.ptEstimate.y;
	}


	//	cout << "prediction : "<<prediction.at<float>(1)<<endl;
	MultiKF.smeasurement(0) = MultiKF.ptCenter.x;
	MultiKF.smeasurement(1) = MultiKF.ptCenter.y;
	MultiKF.smeasurement(2) = MultiKF.speedX;
	MultiKF.smeasurement(3) = MultiKF.speedY;
	MultiKF.smeasurement(4) = rec.width;
	MultiKF.smeasurement(5) = rec.height;

	//	cout << "speed : "<<smeasurement(2)<<", "<<smeasurement(3)<<endl;
	Point measPt(MultiKF.smeasurement(0), MultiKF.smeasurement(1));
	///////////


	Mat estimated = MultiKF.KF.correct(MultiKF.smeasurement);  //measuremnet를 predicted state로 update --------- P구하는듯..
	//!< predicted state (x'(k)): x(k)=A*x(k-1)+B*u(k)
	Point statePt(estimated.at<float>(0), estimated.at<float>(1));//!< corrected state (x(k)): x(k)=x'(k)+K(k)*(z(k)-H*x'(k))---compute the estimate

	//Point statePt(0,0);
	if (imgROImask.at<uchar>((statePt.y - (rec.height / 2)<0 ? 0 : statePt.y - (rec.height / 2)), (statePt.x - (rec.width / 2) < 0 ? 0 : statePt.x - (rec.width / 2))) == 0)
		return false;
	else
	{
		Rect recKalman = Rect(Point2d(statePt.x - (rec.width / 2), statePt.y - (rec.height / 2)), Point2d(statePt.x + (rec.width / 2), statePt.y + (rec.height / 2)));
		//vecValidRec.push_back(recKalman);
		// 안쓰는 부분


		////////////
		//		drawCross(img, statePt, rgb, 5);   // tracking 결과
		//		cv::rectangle(img, recKalman, rgb, 2); //tracking 결과
		//line(video_cap_, Point2d(statePt.x,0), Point2d(statePt.x,240), CV_RGB(255,0,0),3);

		/*line(img, Point2d(statePt.x-1,statePt.y-1), Point2d(statePt.x,statePt.y), rgb,3);
		line(img, Point2d(measPt.x-1,measPt.y-1), Point2d(measPt.x,measPt.y), rgb,3);*/

		Mat prediction = MultiKF.KF.predict();
		//cout << "prediction : " << prediction << endl;
		//putText(img,timeInfo,Point2i(10,30),FONT_HERSHEY_SIMPLEX,0.7,CV_RGB(0,255,0),2);
		MultiKF.ptPredict = Point(prediction.at<float>(0), prediction.at<float>(1));
		//speedX = prediction.at<float>(2);
		//speedY = prediction.at<float>(3);
		//
		//sizePredict.width = prediction.at<float>(4);
		//sizePredict.height = prediction.at<float>(5);
		////
		MultiKF.matPrediction = prediction;

		MultiKF.ptEstimate.x = estimated.at<float>(0);
		MultiKF.ptEstimate.y = estimated.at<float>(1);
		return true;
	}
}


void CKalman::kalmanTrackingStart(StructKalman& MultiKF, Rect& recStart)
{
	MultiKF.KF.statePost.at<float>(0) = recStart.x + (recStart.width) / 2;
	MultiKF.KF.statePost.at<float>(1) = recStart.y + (recStart.height) / 2;
	MultiKF.KF.statePost.at<float>(2) = MultiKF.speedX;
	MultiKF.KF.statePost.at<float>(3) = MultiKF.speedY;
	MultiKF.KF.statePost.at<float>(4) = recStart.width;
	MultiKF.KF.statePost.at<float>(5) = recStart.height;

	MultiKF.KF.statePre.at<float>(0) = recStart.x + (recStart.width) / 2;
	MultiKF.KF.statePre.at<float>(1) = recStart.y + (recStart.height) / 2;
	MultiKF.KF.statePre.at<float>(2) = MultiKF.speedX;
	MultiKF.KF.statePre.at<float>(3) = MultiKF.speedY;
	MultiKF.KF.statePre.at<float>(4) = recStart.width;
	MultiKF.KF.statePre.at<float>(5) = recStart.height;

	SetKalmanParameter(MultiKF.KF, MultiKF.smeasurement);

	MultiKF.smeasurement.at<float>(0) = recStart.x + (recStart.width) / 2;
	MultiKF.smeasurement.at<float>(1) = recStart.y + (recStart.height) / 2;
	MultiKF.width = recStart.width;
	MultiKF.height = recStart.height;
	MultiKF.ptCenter.x = recStart.x + (recStart.width) / 2;
	MultiKF.ptCenter.y = recStart.y + (recStart.height) / 2;
	MultiKF.ptEstimate.x = recStart.x + (recStart.width) / 2;
	MultiKF.ptEstimate.y = recStart.y + (recStart.height) / 2;
	MultiKF.ptPredict.x = MultiKF.ptEstimate.x + MultiKF.speedX;
	MultiKF.ptPredict.y = MultiKF.ptEstimate.y + MultiKF.speedY;
	MultiKF.rgb = Scalar(CV_RGB(rand() % 255, rand() % 255, rand() % 255));

}

CKalman::~CKalman()
{

}

CKalman::CKalman()
{

}




//===============================================================================================================
//KalmanFilter
//
//    Mat statePre;           //!< predicted state (x'(k)): x(k)=A*x(k-1)+B*u(k)
//        - 예측값, 수정 이전 상태값, 초기화
//    Mat statePost;          //!< corrected state (x(k)): x(k)=x'(k)+K(k)*(z(k)-H*x'(k))
//        - 수정값, correct() 함수에 의해 계산됨
//
//    Mat transitionMatrix;   //!< state transition matrix (A)
//        - 변환 행렬, 변환 메커니즘 적용, 중요
//    Mat controlMatrix;      //!< control matrix (B) (not used if there is no control)
//        - 제어 행렬, 초기화 하지 않음
//    Mat measurementMatrix;  //!< measurement matrix (H)
//        - 측정 행렬, correct() 함수에 측정값 입력시 자동 변화
//
//    Mat processNoiseCov;    //!< process noise covariance matrix (Q)
//        - 프로세스 잡음 공분산, 클수록 수정값이 많이 변함, 1e-4
//    Mat measurementNoiseCov;//!< measurement noise covariance matrix (R)
//        - 측정 잡음 공분산, 작을수록 수정값이 많이 변함, 1e-1
//        - 100ms 정도의 지연 시간에서 둘다 1e-3 정도가 적당한 듯
//
//    Mat errorCovPre;        //!< priori error estimate covariance matrix (P'(k)): P'(k)=A*P(k-1)*At + Q)*/
//        - 초기화 안함, 이전 오차 공분산 
//    Mat gain;               //!< Kalman gain matrix (K(k)): K(k)=P'(k)*Ht*inv(H*P'(k)*Ht+R)
//        - 초기화 안함
//    Mat errorCovPost;       //!< posteriori error estimate covariance matrix (P(k)): P(k)=(I-K(k)*H)*P'(k)
//        - 이후 오차 공분산, 초기화 안함
//
//predict();//예측 : 다음 시간단계에 대한 예측, 결과는 statePre에 저장
//correct(Mat measurement);//교정 : 새로운 측정치와 통합, 결과는 statePost에 저장
//
//
//
//
//=============================================================================================================
