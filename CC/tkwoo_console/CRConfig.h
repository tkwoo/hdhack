#include "opencv/cv.h"
#include "opencv/highgui.h"

#include <iostream>
#include <fstream>

using namespace std;
using namespace cv;

class CRConfig{
public:

	string strTreepath;
	int nTrees;
	string strImfiles;
	vector<float> vec_fScale;
	int nPatchWidth;
	int nPatchHeight;
	Size szTrain;

	CRConfig(const char* filename);
	~CRConfig(){}
	void LoadConfig(const char* filename);
};