#include "CRConfig.h"


CRConfig::CRConfig(const char* filename)
{
	LoadConfig(filename);
}


/**
@ Method:    Load configuration file
@ Date:    2015/11/09
@ Returns:   void
@ Parameter: const char * filename
**/
void CRConfig::LoadConfig(const char* filename)
{
	char buffer[400];
	ifstream in(filename);

	if (in.is_open()) {

		// Path to trees
		in.getline(buffer, 400);
		in.getline(buffer, 400);
		strTreepath = buffer;
		//cout << "strTreePath " << strTreepath << endl;

		// Number of trees
		in.getline(buffer, 400);
		in >> nTrees;
		//cout << "strTreePath " << nTrees << endl;

		// Patch size
		in.getline(buffer, 400);
		in.getline(buffer, 400);
		in >> nPatchWidth;
		//cout << "nPatchWidth " << nPatchWidth << endl;
		in >> nPatchHeight;
		//cout << "nPatchHeight " << nPatchHeight << endl;

		//// Path to images
		in.getline(buffer, 400);
		//in.getline(buffer, 400);
		//in.getline(buffer, 400);
		//strImfiles = buffer;
		//cout << "strImfiles " << strImfiles << endl;

		// Scales
		in.getline(buffer, 400);
		int nScaleSize;
		in >> nScaleSize;
		//cout << "nScaleSize " << nScaleSize << endl;

		vec_fScale.resize(nScaleSize);
		for (int i = 0; i < nScaleSize; ++i){
			in >> vec_fScale[i];
			//cout << "vec_fScale[i] " << vec_fScale[i] << endl;
		}
		// Training img width
		in.getline(buffer, 400);
		in.getline(buffer, 400);
		in >> szTrain.width;
		//cout << "szTrain.width " << szTrain.width << endl;

		// Training img height
		in.getline(buffer, 400);
		in.getline(buffer, 400);
		in >> szTrain.height;
		//cout << "szTrain.height " << szTrain.height << endl;

	}
	else {
		cerr << "File not found " << filename << endl;
		exit(-1);
	}
	in.close();

}
