#pragma once

#include "CRForest.h"
#include "KalmanFilter.h"
#include "opencv/cv.h"
#include "opencv/highgui.h"

using namespace cv;

#define ToRadian(degree) ((degree)*(CV_PI/180.0f))
#define ToDegree(radian) ((radian)*(180.0f/CV_PI))

#define IMAGE_WIDTH 1280//6
#define IMAGE_HEIGHT 720//6

#define ALPHA 6//6
#define LOCAL_THRESHOLD 1

#define DISCRIMINATIVE_VOTING_1st 0
#define DISCRIMINATIVE_VOTING_2nd 0

#define Debug 0
#define TimeCheck 1

#define EVALUATE 0
#define EVAL_CLASSIFIER 0
#define EVAL_POS 0
#define EVAL_NEG 0

#define STORE_IMAGME_CROP 0
#define STORE_IMAGE_NEG_CROP 0

#define STORE_GTnDT_TXT 0
#define RECORD 0

enum eDetectObject{
	car_rear = 0,
	car_cutin = 1
};

enum eDetectionMode
{
	global_roi = 0,
	local_roi = 1,
	eval_roi = 2
};

struct sPointInfo
{
	int nX;
	int nY;
	float nIntensity;
	float fScale;
};

struct sDetectLocal{
	float scale;
	Mat mROI;
};

struct sMeanStddev{
	double dMean;
	double dStddev;
};

struct sKITTI_GT{
	string type;
	float fAlpha;
	int nOcclusion;
	Rect rectBB;
};

struct sOutput{
	int GT;
	int TP;
	int TN;
	int FP;
	int FN;
};

typedef struct vecTracking{
	vector<Rect> vecBefore;
	vector<Rect> vecCandidate;
	vector<int> vecCount;
	vector<Point> vecCountPush;
	vector<Point2f> vecRtheta;
	vector<Point2f> vecAngle;
}Track_t;


class CRForestDetector{
private:
public:
#if STORE_GTnDT_TXT
	FILE* fGT, *fDT;
	vector<float> vec_fMaxHoughIntensity;
	float fMaxHoughIntensity;
#endif

	//const CRForest* crForest;
	CRForest* crForest;
	CKalman *cKalman;

	string strTreepath;
	int nTrees;
	string strImfiles;
	vector<float> vec_fScale;
	int nPatchWidth;
	int nPatchHeight;
	Size szTrain;

	int nStrideX;
	int nStrideY;

	Point ptVanishing;
	float fGlobalThreshold;

	// 트랙커 정보
	float fScaleDist;
	int nCntCandidate;
	int nCntBefore;
	int nFrameCandiate;
	int nAngleThresh;
	Mat imgTrackingRegion;
	// tracker parameter
	Track_t High;

	// Evaluation
	vector<vector<sKITTI_GT>> vec_sKITTI_GT;
	vector<vector<Rect>> vecGT;
	sOutput outDetection;
	vector<int> vecGTObjIdList;
	vector<int> vecDetectObjIdList;

	// flag
	bool bStartOfVideo;

	//  칼만필터 정보
	// tracking vector
	vector<StructKalman> MultiKF;

	// 프레임 정보
	int nFrameIdx;

	// feature 이미지 벡터
	vector<Mat> vec_imgFeature;

	// 전역적 ROI 정보
	vector<Point> vec_ptROI;
	// 전역적 ROI들의 bottom right.y 좌표 저장 벡터
	vector<int> vec_nRoiTlY;

	// feature 추출 영역, tracking 수행 영역
	Rect rectFeatureExtractRegion, rectTrackROI;

	// 전역적 ROI의 사이즈 저장 벡터
	vector<Size> vec_szROI;

	// 지역적 ROI의 허프맵 저장 벡터
	vector<sDetectLocal> vec_sLocalHoughMap;

	// 전역적 ROI의 사이즈 저장 벡터
	vector<Rect_<int>> vec_rectROI;

	// 전역적 ROI의 허프맵 저장 벡터
	vector<Mat> vec_imgGlobalHough;

	// 차량 검출 후보 저장 벡터 
	vector<Rect> vec_rectCandidates;	// found candidates

	// 차량 검출 Score 저장 벡터
	vector<float> vec_fCandidates;

	// 지역적 ROI 저장 벡터  
	vector<Rect> vec_rectLocalROIs;	// ex) stixel roi
	vector<sDetectLocal> vec_sLocalROIs;

	// Mean 이미지 저장 벡터
	vector<Mat> vec_imgMeanFeature;

	// Stdev 이미지 저장 벡터
	vector<Mat> vec_imgStdevFeature;

	//// 비디오 파라미터
	//VideoCapture m_CapVideo;

	// 트랙킹 ROI
	Rect_<int> rect_TROI;

	//vector<Mat> ROIHoughMap;

	vector<vector<vector<vector<Point2i>>>> vec_HoughOffsetDerive;

	void GenerateHoughMapInGlobalROI(const vector<Mat>& vec_imgFeature, Mat& imgHough);

	// 트랙커 초기값 설정함수
	void InitTrackerInfo();

	// 칼만필터를 이용한 트랙킹
	void KalmanMultiTracker(Track_t & Set, vector<StructKalman>& MultiKF, /*float scaledist, int cntCandidate, int cntBefore, int frameCandiate,*/ int& nFrameIdx/*, vector<Rect>& vecValidRec, Mat& imgROImask, CRForestDetector& crDetect*/);

	float AngleTransform(const float &tempAngle, const int &scale){
		return (float)CV_PI / 180 * (tempAngle*scale);
	}

	float fAngle(int x, int y)
	{
		return (float)ToDegree(atan2f(y, x));
	}

	float Rounding(float fVal, int nDigit){
		return (floor(fVal*pow(float(10), nDigit) + 0.5f) / pow(float(10), nDigit));
	}
	bool bIsGlobalROI;

	Mat imgSrc;

	// Constructor
	CRForestDetector(const CRConfig& crCF);
	CRForestDetector(const CRConfig& crCF, /*const*/ CRForest& cRF, CKalman& cKM);
	~CRForestDetector();

	unsigned int GetNumCenter() const { return crForest->GetNumCenter(); }

	// 소실점 설정
	void setVanishingPoint(int nX, int nY);

	// 초기화 함수
	void Initializer();

	// ROI 위치 초기화
	void InitializeROI();
	void InitializeROIBasedOnDist();

	// scale 설정
	void InitializeScale();

	//// 비디오 프레임 세팅
	//bool SetTestVideo();

	//// 비디오 프레임 로드
	//bool LoadTestVideo();

	//void GenerateNoramlizedImage();

	// 허프맵의 intensity 값들을 x축 방향으로 projection 시키는 함수
	void ProjectImage(Mat& img, vector<float>& vecXProject, vector<float>& vecYProject);

	// 허프맵의 peak들을 추출
	void FindMaximum(Mat& imgSrc, float nThreshold, float fScale, vector<sPointInfo>& vecPoint);
	void FindMaximum2(Mat& imgSrc, float fScale, vector<sPointInfo>& vecPoint);

	//Feature 이미지로부터 ROI 영역 자름
	void CropROIFromFeature(Mat& imgFeature, vector<Mat>& vec_imgFeature);

	// 허프맵 생성
	void GenerateHoughMapInPyramid();

	// 허프맵 생성
	void GenerateHoughMapForEval();

	// 허프맵의 peak들로부터 자동적으로 임계값 계산
	void FindAutomaticThreshold();
	void FindAutomaticThreshold2();

	// 지역적 ROI로부터 허프맵 계산
	void GenerateHoughMapInLocalROI(Rect& rectFeatureRg);

	// 지역적 ROI에 대한 feature 추출 영역 계산
	void CalculateFeatureExtractRegion();

	// 전역적 ROI로부터 차량 중심점 위치계산 
	void LocalizeGlobalVehicleCenter(Point pt, vector<Point>& ptMaxInROIs, vector<int>& vec_includedROIIdx);

	// 지역적 ROI로부터 차량 중심점 위치계산 
	void LocalizeLocalVehicleCenter();

	// peak의 intensity에 따른 영상 내 차량 사이즈 계산 
	void CalculateProperBoxSize(vector<Point>& ptMaxInROIs, Rect& rectIPBox, vector<int>& vec_includedROIIdx);

	// cluster 군집화
	void GetClusters(vector<Rect>& vecDetected, vector <vector< Rect >> &vecGroup);

	// 사각형 군집화
	void ClusterRect(vector<Rect>& vecClustered);

	// Feature 추출할 영역 반환
	Rect getFeatureExtractRegion(){
		return rectFeatureExtractRegion;
	}

	// ROI로부터 feature 추출
	void extractFeatureChannels(Mat& img, std::vector<Mat>& vImg);

	// min, max filter에 의한 feature를 추출
	Mat MinFilter2(Mat& imgSrc, int nX, int nY);
	Mat MaxFilter2(Mat& imgSrc, int nX, int nY);

	// 차량 검출
	void DetectVehicle(Mat &imgVideoInput);

	// 차량 Score 저장
	void SetVehicleScore(vector<Rect>& vec_rectCandi, vector<float>& vec_fCandi);

	// 전역적 ROI에서 차량 검출
	void DetectInFullROI();

	// 지역적 ROI에서 차량 검출
	void DetectInLocalROI();

	// 전역적 ROI에서 차량 후보 검출
	void FindVehicleCandidate();

	// 허프맵의로부터 peak 계산
	void FindPeaksInHough(vector<Point> &vec_gloabalPts);

	// 차량 사이즈 계산
	void CalculateVehicleSize(vector<Point> &vec_gloabalPts, vector<Rect> &vec_rectDetected);

	// 차량 후보 군집화
	void GroupVehicleCandidates(vector<Rect> &vec_rectDetected);

	// 지역적 ROI 세팅
	void SetLocalROIs(vector<Rect>&  vec_rectROIs);

	// 검출 차량박스 반환
	void getVehicleCandidates(vector<Rect>& vec_rectCandidates);

	// Mean, Stdev 이미지 로드 함수
	void LoadMeanNStdevImage();

	// discriminative hough map을 그리는 함수 
	void DrawDiscriminativeHoughMap(vector<Mat>& vec_imgDiscriminativeHM);

	// 검출 차량박스 출력
	void ShowVehicles(Mat& imgDisp);

	// KITTI GT 로드 (txt)
	bool LoadKITTIGT(const char* filename);

	// CVLAB GT 로드 (txt)
	bool LoadCVLABGT(const char* filename);

	// KITTI GT 출력 
	void DrawKITTIGT(int nIdx, Mat& imgDisp);

	// CVLAB GT 출력
	void DrawCVLABGT(int nIndx, Mat& mInput);

	// KITTI GT 출력 
	void EvaluateKITTIRear(int nIdx, Mat& imgDisp);

	void InitializeOutput(sOutput& out);

	void EvaluatePerformance(vector<Rect>& vecDetected, int nIndx, sOutput& out);

	void PrintEvaluateResult(sOutput& out);

	bool LoadKITTIObjData(const string& strTestDataPath, int& nDataIdx, Mat& imgSrc);

	void FilterGT(int nIdx);

	void DetectForEval(Mat &imgInput, float& fThreshold, sOutput &sDetection);

	bool CheckTrueForEval(float &fThreshold);

	// 버퍼 clear
	void ClearMemory();
	
	/////Integration
	void GetResultRects(vector<Rect>&  vec_rectBB);
	void GetResultScores(vector<float>&  vec_fScore);
	////////////////////////////////////////////////////////////////////

	//// 비디오 메모리 반환
	//void ReleaseViedo();
};
