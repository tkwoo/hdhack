#include<iostream>

#include "CRForestDetector.h"

using namespace std;

class CRForestDetectorCI : public CRForestDetector
{
public:
	CRForestDetectorCI(const CRConfig& crCF, /*const*/ CRForest& cRF, CKalman& cKM);
	//CRForestDetectorCI();
	~CRForestDetectorCI();

	void InitializerCI();
	void DetectVehicle(Mat& imgSrc);
	void DetectInCutInROI();
	void LocalizeCutInVehicleCenter();

	//string strImfiles;
	//vector<float> vec_fScale;
	//int nPatchWidth;
	//int nPatchHeight;
	//Size szTrain;

	/*const CRForest* crForest;
	CKalman *cKalman;*/

private:
	void InitializeROICI();
	void GenerateHoughMapPyramidCI();
	void FindVehicleCandidateCI();
	void FindPeaksInHoughCI(vector<Point> &vec_gloabalPoints);
	void LocalizeGlobalVehicleCenterCI(Point pt, vector<Point>& ptMaxInROIs);
	void CalculateVehicleSizeCI(vector<Point> &vec_gloabalPts, vector<Rect> &vec_rectDetected);
	void CalculateProperBoxSizeCI(vector<Point>& ptMaxInROIs, Rect& rectIPBox);
	void ShowVehicleCI(Mat& imgDisp);
	void ClearMemoryCI();

	vector<Rect> vec_rectFeatureExtractRegion;
	vector<Rect> vec_rectTrackROI;
};
