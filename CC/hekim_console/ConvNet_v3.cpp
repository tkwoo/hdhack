#include "general_settings.h"
#include "readXml.h"
#include <time.h>
using namespace cv;
using namespace std;

std::vector<ConvLayerConfig> convConfig;
std::vector<FullConnectLayerConfig> fcConfig;
SoftmaxLayerConfig softmaxConfig;
vector<int> sample_vec;
///////////////////////////////////
// General parameters
///////////////////////////////////
bool is_gradient_checking = false;
bool use_log = false;
int log_iter = 0;
int batch_size = 1;
int pooling_method = 0;
int non_linearity = 2;
int training_epochs = 0;
double lrate_w = 0.0;
double lrate_b = 0.0;
int iter_per_epo = 0;

vector<Cvl> ConvLayers;
vector<Fcl> HiddenLayers;
Smr smr;

void initRead(char * Xmlfilename)
{
	int imgDim = 32;
	int nsamples = 1;

	readConfigFile("config.txt");
	ConvNetInitPrarms(ConvLayers, HiddenLayers, smr, imgDim, nsamples);
	readxml(ConvLayers, HiddenLayers, smr, Xmlfilename);
}


int cnnMain(vector<Mat>& testX, Mat& result)
{
	result = resultPredict(testX, ConvLayers, HiddenLayers, smr);

	/////////////////////////////////////////////////////////////////

	return 0;
}

int cnnScore(vector<Mat>& testX, Mat& result, Mat& imgScore)
{
	result = resultPredictScore(testX, ConvLayers, HiddenLayers, smr, imgScore);

		/////////////////////////////////////////////////////////////////

	return 0;
}