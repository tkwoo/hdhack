#pragma once

#include "TSD.h"
#include "TST.h"
#include "TSC.h"

#define TRAIN_SIZE 32


//bool LoadVideoFrame(Mat& imgInput);
//void SetVideoFile(const string& strVideoPath);
void ClusterRectangle(vector<Rect>& vec_rectInput);
void GenerateCandidate(Mat& imgSrc, vector<Rect>& vec_rectCandidate);
void ClusterPlateRegion(vector<Rect>& vec_rectCandidate, Rect& ROI, vector<Rect>& vec_rectPlateRegion);
void RecognizePlateNumber(Mat& imgGray, Rect& ROI, vector<Rect>& vec_rectPlateRegion, vector<int>& vec_nPlateNumRst, vector<double>& vec_nPlateNumScore);
void VisualizeRegion(Mat& imgSrc, vector<Rect> vec_rectRegion, Rect& ROI, vector<int>& vec_nResult, vector<double>& vec_nPlateNumScore);
void ReturnPlateNumStr(vector<int>& vec_nResult, char* strPlateNum);
void PlateNumberRecognition(Mat& imgSrc, char* strPlateNum);