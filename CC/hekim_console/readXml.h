#pragma once
#include "general_settings.h"
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;
using namespace cv;



void writexml(vector<Cvl> &CLayers, vector<Fcl> &hLayers, Smr &smr, char *xmlfile);
void readxml(vector<Cvl> &CLayers, vector<Fcl> &hLayers, Smr &smr, char *xmlfile);