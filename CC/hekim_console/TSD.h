#pragma once
#include <iostream>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <cv.h>       
#include <ml.h>		  // opencv machine learning include file
#include "opencv/highgui.h"
//#include <io.h>
#include "structure.h"
using namespace std;
using namespace cv;

/**
* @brief Traffic sign detection class
* @details 표지판 검출 클래스
* @author Cheolyong Jang
* @date 2015-12-22
* @version 1.0.0
*/
class CTSD
{
public:
	CTSD();
	CTSD(Rect , int , int , float );
	~CTSD();

	void SetROI(Rect);
	void SetMinArea(int);
	void SetMaxArea(int);
	void SetScale(float);
	Rect GetROI() const;
	int GetMinArea() const;
	int GetMaxArea() const;
	float GetScale() const;

	/**
	* @brief traffic sign detection function
	*/
	void DetectSigns(Mat& srcImage, SVecMSERs& MserVec, vector<Rect>& vecRectTracking); 
	/**
	* @brief detected bounding box clustering
	*/
	void Clustering(vector<Rect>& vecCluster, int distance);

	CvNormalBayesClassifier *m_bayesian;
	CvSVM* m_svm = new CvSVM;

private:
	/**
	* @brief clolor filter function
	*/
	void ColorMask(SImgMask& CM, Mat& imgHue, Mat& imgRGB);
	/**
	* @brief acromatic filter function
	*/
	void Acromatic(Mat& imgRGB, Mat& imgMask, float fvalue, uchar uThreshold);
	/**
	* @brief color enhancement function
	*/
	void Enhancement(Mat imgRGB, Mat& imgDstRed, Mat& imgDstBlue);
	/**
	* @brief MSERsegmentation & detect signs using bayesian classifier
	*/
	void MSERsegmentation(MSER& mser, Mat& imgInputMSER, Mat& imgOrigin, SImgMask& CM, vector<Rect>& vecRect, SImgAcromatic& AM, SVecMSERs& MserVec, Category category);
	/**
	* @brief rotate image
	*/
	void rotate(Mat& src, double angle, Mat& dst);

	/**
	* @brief DtBs feature vector
	*/
	void HistVec(Mat& imghistor, Mat &matHist);

	/**
	* @brief bayesianclassifier function
	*/
	int BayesianClassifier(CvNormalBayesClassifier *bayes, Mat & imghistor);
	
	int SVMClassifier(CvSVM* svm, Mat& imghistor);

	void HOG_DtB_Extractor(Mat &x, Mat& PN_Descriptor_mtx);

	void HistVec(Mat& imghistor, vector<float>& vecHist);



	int m_cntmser = 0;
	float m_fscale;
	Rect m_rectROIset;
	int m_nMinArea;
	int m_nMaxArea;
	Mat m_imgSrc;
	Mat m_imgdrawTest;
	SRecSize m_ObjSize;

	Category m_category;
};

