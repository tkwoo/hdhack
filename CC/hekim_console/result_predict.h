#pragma once
#include "general_settings.h"

using namespace std;
using namespace cv;

Mat resultPredict(const vector<Mat> &, const vector<Cvl> &, const vector<Fcl> &, const Smr &);
Mat resultPredictScore(const vector<Mat> &, const vector<Cvl> &, const vector<Fcl> &, const Smr &, Mat& imgScore);
void testNetwork(const vector<Mat> &, const Mat&, const vector<Cvl> &, const vector<Fcl> &, const Smr &);
void testNetwork2(const vector<Mat> &testX, const Mat &testY, const vector<Cvl> &CLayers, const vector<Fcl> &hLayers, const Smr &smr);