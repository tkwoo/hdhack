#include <cv.h>
#include <highgui.h>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

int main(){
	string str;
	ifstream file("gps_new.txt");
	char* ptr;
	char buffer[1024];
	vector<float> vec_fGPSX_diff;
	vector<float> vec_fGPSY_diff;

	// Load GPS data
	if (file.is_open()){
		while (file.getline(buffer, 400)){

		/*	cout << buffer << endl;
			cout << "check" << endl;*/

			char *p = strtok(buffer, "\t");
			float fGPSX = stof(p);
			p = strtok(NULL, "\t");
			float fGPSY = stof(p);

			p = strtok(NULL, "\t");
			float fGPSX2 = stof(p);
			p = strtok(NULL, "\t");
			float fGPSY2 = stof(p);

			float fGPSX_diff = fGPSX2 - fGPSX;
			float fGPSY_diff = fGPSY2 - fGPSY;

			vec_fGPSX_diff.push_back(fGPSX_diff);
			vec_fGPSY_diff.push_back(fGPSY_diff);
		}

	}
	else{
		file.close();
	}

	int nCnt = 0;
	// Draw Rect
	while (nCnt != vec_fGPSY_diff.size()){
		
		
		/*Mat imgDraw = Mat(Size(500, 500), CV_8UC1);

		rectangle(imgDraw, Rect(250 - vec_fGPSX_diff[nCnt], 250 - vec_fGPSY_diff[nCnt], 50, 100), Scalar(0), 2);
		imshow("imgDraw", imgDraw);
		cvWaitKey(1);*/
		nCnt++;
	}
	return 0;

}
