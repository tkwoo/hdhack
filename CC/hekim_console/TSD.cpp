#include "TSD.h"





CTSD::CTSD()
{
	SetScale(1.0);
	SetROI(Rect(0,0,0,0));
	SetMinArea(0);
	SetMaxArea(0);
}
CTSD::CTSD(Rect rectROIset, int nMinArea, int nMaxArea, float fscale = 1.0)
{
	SetScale(fscale);
	SetROI(rectROIset);
	SetMinArea(nMinArea);
	SetMaxArea(nMaxArea);
	
}


CTSD::~CTSD()
{
}

void CTSD::SetROI(Rect _rec)
{
	m_rectROIset = _rec;
}

void CTSD::SetMinArea(int _area)
{
	m_nMinArea = _area;
	m_ObjSize.m_minRectAreaRED = m_nMinArea*GetScale();
	m_ObjSize.m_minRectAreaBLUE = m_nMinArea*GetScale();
	m_ObjSize.m_minRectAreaBLACK = m_nMinArea*GetScale();

}

void CTSD::SetMaxArea(int _area)
{
	m_nMaxArea = _area;
	m_ObjSize.m_maxRectAreaRED = m_nMaxArea*GetScale();
	m_ObjSize.m_maxRectAreaBLUE = m_nMaxArea*GetScale();
	m_ObjSize.m_maxRectAreaBLACK = m_nMaxArea*GetScale();
}
void CTSD::SetScale(float _scale)
{
	m_fscale = _scale;
}

Rect CTSD::GetROI() const{
	return m_rectROIset;
}

int CTSD::GetMinArea() const{
	return m_nMinArea*GetScale();
}

int CTSD::GetMaxArea() const{
	return m_nMaxArea*GetScale();
}

float CTSD::GetScale() const{
	return m_fscale;
}



void CTSD::DetectSigns(Mat& srcImage, SVecMSERs& MserVec, vector<Rect>& vecRectTracking)
{
	//m_imgSrc = srcImage.clone();
	double time1 = (double)getTickCount();

	int bBlack = 0;
	vector<Rect> vecRect;

	double time2 = (double)getTickCount();
	Mat imgInput = srcImage(GetROI());
	if (GetScale() < 1.0)
		resize(imgInput, imgInput, Size(cvRound(GetScale()*imgInput.cols), cvRound(GetScale()*imgInput.rows)), 0, 0, INTER_CUBIC);

	/////////////  HSV  /////////////////
	Mat imgHSV;
	Mat imgYCrCb;
	Mat imgDst;
	Mat imgGRAY;
	cvtColor(imgInput, imgGRAY, CV_BGR2GRAY);
	cvtColor(imgInput, imgHSV, CV_BGR2HSV);
	vector<Mat> vecHSV;
	split(imgHSV, vecHSV);

	

	vector<Mat> vecRGB;
	split(imgInput, vecRGB);

	Mat imgEnhanceR(imgInput.size(), CV_8UC1);
	Mat imgEnhanceB(imgInput.size(), CV_8UC1);
	Enhancement(imgInput, imgEnhanceR, imgEnhanceB);

	
	medianBlur(imgEnhanceR, imgEnhanceR,3);
	//imgEnhanceR += imgEnhanceB;

	imshow("imgEnhanceB", imgEnhanceB);
	//imshow("imgEnhanceR", imgEnhanceR);


	/////////////////////////////////////////////////////////////////////////////////

	SImgAcromatic AM;

	AM.m_imgAcroMaskBlue = Mat::zeros(imgInput.rows, imgInput.cols, CV_8UC1);
	AM.m_imgAcroMaskRed = Mat::zeros(imgInput.rows, imgInput.cols, CV_8UC1) + 255;

	Acromatic(imgInput, AM.m_imgAcroMaskBlue, 10, 30);
	Acromatic(imgInput, AM.m_imgAcroMaskRed, 10, 30);

	//imshow("imgAcroMaskBlue",imgAcroMaskBlue);
	//imshow("imgAcroMaskRed",imgAcroMaskRed);

	/////////////////////////////////////////////////////////////////////////////////
	//////////////////////////  ColorMask  //////////////////////////////////////////
	SImgMask CM;
	CM.m_imgRedMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
	CM.m_imgBlueMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
	CM.m_imgGreenMask=Mat::zeros(vecHSV[0].rows,vecHSV[0].cols,CV_8UC1);
	ColorMask(CM, vecHSV[0], imgInput);

	medianBlur(CM.m_imgRedMask, CM.m_imgRedMask, 3);
	//imshow("CM.m_imgRedMask", CM.m_imgRedMask);
	bitwise_and(CM.m_imgBlueMask, AM.m_imgAcroMaskBlue, CM.m_imgBlueMask);


	////////////////////////////////////////////////////////////////////////////////
	//////////////////////// Adaptive Threshold ////////////////////////////////////
	Mat imgAdapThres;
	adaptiveThreshold(vecHSV[2], CM.m_imgAdapThres, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY_INV, 31, 5);

	////////////////////////////////////////////////////////////////////////////////
	//imgEnhanceR += imgEnhanceB;
	
	time1 = (double)getTickCount() - time1;
	//printf( "imgprocessing : %f ms.\n", time1*1000./getTickFrequency() );

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////// MSER ////////////////////////////////////////////////

	imgInput.copyTo(m_imgdrawTest);

	Mat imgProb = Mat::zeros(vecHSV[1].rows, vecHSV[1].cols, CV_8UC1);

	// MSERs segmentation
	MSER mserSaturation(7, 30, 3500, 0.5, 0.1, 250, 1.01, 0.003, 5);
	MSERsegmentation(mserSaturation, vecHSV[2], imgInput, CM, vecRect, AM, MserVec, allsign); // S channel
	MSER mserGray(7, 30, 3500, 0.2, 0.1, 250, 1.01, 0.003, 5);
	//MSERsegmentation(mserSaturation, vecHSV[1], imgInput, CM, vecRect, AM, MserVec, allsign); // S channel
	MSERsegmentation(mserGray, imgEnhanceR, imgInput, CM, vecRect, AM, MserVec, redsign); //Red enhance channel
	MSERsegmentation(mserGray, imgEnhanceB, imgInput, CM, vecRect, AM, MserVec, bluesign); //Blue enhance channel
	
	imshow("m_imgdrawTest", m_imgdrawTest);

	//Clustering(vecRect, 10);
	time2 = (double)getTickCount() - time2;
	printf("MSER detection : %f ms.\n", time2*1000. / getTickFrequency());

	// ROI에 따라 바뀐 image 좌표 계산후 다시 입력
	{
		for (int i = 0; i < vecRect.size(); i++)
		{
			Rect rec = Rect(Point(cvRound(vecRect[i].x / GetScale() + GetROI().x), cvRound(vecRect[i].y / GetScale() + GetROI().y)), Point(cvRound((vecRect[i].x + vecRect[i].width) / GetScale() + GetROI().x), cvRound((vecRect[i].y + vecRect[i].height) / GetScale() + GetROI().y)));
			vecRectTracking.push_back(rec);
		}
	}

		
	vecRect.clear();
}

void CTSD::ColorMask(SImgMask& CM, Mat& imgHue, Mat& imgRGB)
{
	vector<Mat> vecRGB;
	split(imgRGB, vecRGB);
	for (int i = 0; i < imgHue.rows*imgHue.cols; i++)
	{
		if ((imgHue.at<uchar>(i)>=0 && imgHue.at<uchar>(i)<=10) || (imgHue.at<uchar>(i)>150 && imgHue.at<uchar>(i)<=180) /* || (abs(vecRGB[0].at<uchar>(i)-vecRGB[1].at<uchar>(i))<5 && abs(vecRGB[1].at<uchar>(i)-vecRGB[2].at<uchar>(i))<5 && abs(vecRGB[2].at<uchar>(i)-vecRGB[0].at<uchar>(i))<5)*/)
			CM.m_imgRedMask.at<uchar>(i) = 255;
		else
			CM.m_imgRedMask.at<uchar>(i) = 0;

		if (imgHue.at<uchar>(i)>100 && imgHue.at<uchar>(i)<125)
			CM.m_imgBlueMask.at<uchar>(i) = 255;
		else
			CM.m_imgBlueMask.at<uchar>(i) = 0;

		//if(imgHue.at<uchar>(i)>20 && imgHue.at<uchar>(i)<90 && (vecRGB[0].at<uchar>(i)<220 || vecRGB[1].at<uchar>(i)<220 || vecRGB[2].at<uchar>(i)<220))
		if (imgHue.at<uchar>(i)>20 && imgHue.at<uchar>(i) < 90 && (vecRGB[0].at<uchar>(i) < 220 && vecRGB[1].at<uchar>(i) < 230 && vecRGB[2].at<uchar>(i) < 220))
			CM.m_imgGreenMask.at<uchar>(i) = 255;
		else
			CM.m_imgGreenMask.at<uchar>(i) = 0;



	}
}

void CTSD::Acromatic(Mat& imgRGB, Mat& imgMask, float fvalue, uchar uThreshold)
{
	float facromatic = 0;
	vector<Mat> vecRGB;
	split(imgRGB, vecRGB);
	int cols = imgRGB.cols;
	for (int i = 0; i<imgRGB.rows; i++)
	{
		for (int j = 0; j<imgRGB.cols; j++)
		{
			//float sum =(abs((float)vecRGB[0].at<uchar>(i*cols+j) - (float)vecRGB[1].at<uchar>(i*cols+j)) + abs((float)vecRGB[0].at<uchar>(i*cols+j) - (float)vecRGB[2].at<uchar>(i*cols+j)-10)+abs((float)vecRGB[1].at<uchar>(i*cols+j) - (float)vecRGB[2].at<uchar>(i*cols+j)-10));
			float sum = (abs((float)vecRGB[0].at<uchar>(i*cols + j) - (float)vecRGB[1].at<uchar>(i*cols + j)) + abs((float)vecRGB[0].at<uchar>(i*cols + j) - (float)vecRGB[2].at<uchar>(i*cols + j)) + abs((float)vecRGB[1].at<uchar>(i*cols + j) - (float)vecRGB[2].at<uchar>(i*cols + j)));
			facromatic = sum / (3 * fvalue);
			//cout<<"facromatic : "<< sum<<endl;
			if (facromatic <1)
				imgMask.at<uchar>(i*cols + j) = 0;
			else if (facromatic >= 1 || (vecRGB[0].at<uchar>(i*cols + j)<uThreshold&&vecRGB[1].at<uchar>(i*cols + j)<uThreshold&&vecRGB[2].at<uchar>(i*cols + j)<uThreshold))
				imgMask.at<uchar>(i*cols + j) = 255;

		}
	}


}

void CTSD::Enhancement(Mat imgRGB, Mat& imgDstRed, Mat& imgDstBlue)
{
	vector<Mat> vecimgRGB;
	split(imgRGB, vecimgRGB);
	Mat imgR, imgG, imgB;
	vecimgRGB[2].convertTo(imgR, CV_32FC1);
	vecimgRGB[1].convertTo(imgG, CV_32FC1);
	vecimgRGB[0].convertTo(imgB, CV_32FC1);
	Mat imgTempR(imgDstRed.size(), CV_32FC1);
	Mat imgTempB(imgDstRed.size(), CV_32FC1);

	for (int i = 0; i < imgDstRed.cols; i++)
	{
		for (int j = 0; j < imgDstRed.rows; j++)
		{
			imgTempR.at<float>(j, i) = (float)(255.0 * 2.0 * max(0.0f, (min(imgR.at<float>(j, i) - imgG.at<float>(j, i), imgR.at<float>(j, i) - imgB.at<float>(j, i)) / (imgR.at<float>(j, i) + imgG.at<float>(j, i) + imgB.at<float>(j, i)))));
			//imgTempB.at<float>(j, i) = (float)(255.0 * 2.0 * max(0.0f, (max(imgB.at<float>(j, i) - imgG.at<float>(j, i), imgB.at<float>(j, i) - imgR.at<float>(j, i)) / (imgR.at<float>(j, i) + imgG.at<float>(j, i) + imgB.at<float>(j, i)))));
			imgTempB.at<float>(j, i) = (float)(255.0 * 2.0 * max(0.0f, (imgB.at<float>(j, i) - imgR.at<float>(j, i)) / (imgR.at<float>(j, i) + imgG.at<float>(j, i) + imgB.at<float>(j, i))));
			//cout << imgTemp.at<float>(j, i)*255 << " ";
		}
	}

	imgTempR.convertTo(imgDstRed, CV_8UC1);
	imgTempB.convertTo(imgDstBlue, CV_8UC1);

}

void CTSD::Clustering(vector<Rect>& vecCluster, int distance)
{


	for (int j = 0; j < vecCluster.size(); j++)
	{
		for (int k = j + 1; k < vecCluster.size(); k++)
		{
			Rect overRec = vecCluster[j] & vecCluster[k];
			if (overRec.area() == vecCluster[k].area() && vecCluster[j].area() > overRec.area() * 2)
			{
				vecCluster.erase(vecCluster.begin() + k);
				--k;
			}
			else if (overRec.area() == vecCluster[j].area() && vecCluster[k].area() > overRec.area() * 2)
			{
				vecCluster.erase(vecCluster.begin() + j);
				--j;
			}
		


		}
	}







	for (int i = 0; i< vecCluster.size(); i++)
	{
		int cntSame = 0;
		for (int j = i + 1; j<vecCluster.size(); j++)
		{
			if (i == j)
				continue;
			int recX = abs(vecCluster[i].x - vecCluster[j].x);
			int recY = abs(vecCluster[i].y - vecCluster[j].y);
			int recW = abs(vecCluster[i].x + vecCluster[i].width - vecCluster[j].x - vecCluster[j].width);
			int recH = abs(vecCluster[i].y + vecCluster[i].height - vecCluster[j].y - vecCluster[j].height);


			if ((recX<distance && recY< distance && recW <distance && recH <distance) || (recX + recY + recW + recH)<distance * 3)
			{
				cntSame++;

				vecCluster[i].x = (int)(vecCluster[i].x*(cntSame + 1) + vecCluster[j].x) / (cntSame + 2);
				vecCluster[i].y = (int)(vecCluster[i].y*(cntSame + 1) + vecCluster[j].y) / (cntSame + 2);
				vecCluster[i].width = (int)(vecCluster[i].width*(cntSame + 1) + vecCluster[j].width) / (cntSame + 2);
				vecCluster[i].height = (int)(vecCluster[i].height*(cntSame + 1) + vecCluster[j].height) / (cntSame + 2);
				vecCluster.erase(vecCluster.begin() + j);
				j--;
			}
		}
		cntSame = 0;
	}
}

void CTSD::MSERsegmentation(MSER& mser, Mat& imgInputMSER, Mat& imgOrigin, SImgMask& CM, vector<Rect>& vecRect, SImgAcromatic& AM, SVecMSERs& MserVec, Category category)
{
	vector<vector<Point> > points;
	mser(imgInputMSER, points);
	Mat imgProb = Mat::zeros(imgInputMSER.rows, imgInputMSER.cols, CV_8UC1);
	uchar* probdata = (uchar*)imgProb.data;
	MserVec.m_vecPoints.insert(MserVec.m_vecPoints.begin() + MserVec.m_vecPoints.size(), points.begin(), points.end());
	
	double t = (double)getTickCount();

	m_cntmser++;
	Rect rec;
	for (int i = 0; i < (int)points.size(); i++)
	{


		rec = boundingRect(points.at(i));
		Rect recPush = rec;
		recPush.x = recPush.x / m_fscale + GetROI().x;
		recPush.y = recPush.y / m_fscale + GetROI().y;
		recPush.width = (float)recPush.width / m_fscale;
		recPush.height = (float)recPush.height / m_fscale;

		// 모든 MSER로 segmentation된 rect 저장
		MserVec.m_vecRectMser.push_back(recPush);

		//////////////////////////////////////////
		Mat imgTempMser;
		vector<Point>& r = points[i]; //
		for (int j = 0; j < (int)r.size(); j++)
		{
			Point pt = r[j];
			probdata[pt.y*imgProb.cols + pt.x] = 255;
			//imgProb.at<uchar>(pt) = 255;

		}

		Mat imgTempProb;
		imgProb.copyTo(imgTempProb);
		imgTempMser = imgTempProb(rec);
		imgProb.setTo(0);

	
		//////////////////////////////////////
		//adaptive threshold한 부분과 blob이 겹치는지 count
		int cntadapt = 0;
		uchar* adaptdata = (uchar*)CM.m_imgAdapThres.data;
		for (int j = 0; j<points[i].size(); j++){
			if (adaptdata[points[i][j].y*CM.m_imgAdapThres.cols + points[i][j].x] != 0)
				cntadapt++;
		}
		//////////////////////////


		if (rec.x>2)
			rec.x -= 2;
		if (rec.y>2)
			rec.y -= 2;
		if (rec.br().x+ 4<imgInputMSER.cols)
			rec.width += 4;
		if (rec.br().y + 4<imgInputMSER.rows)
			rec.height += 4;

		if (rec.x<2 || rec.y<3 || (rec.br().x)>imgOrigin.cols - 2)
			continue;

		// red sign
		if (category == redsign || category == allsign){
			/////////////////////////////
			int count = 0;
			int cntred = 0;
			int cntacro = 0;

			// red mask 영역 count
			Mat imgBlock = CM.m_imgRedMask(rec);
			uchar* blockdata = (uchar*)imgBlock.data;
			for (int j = 0; j<imgBlock.cols*imgBlock.rows; j++)
			if (blockdata[j]>0)
				count++;

			uchar* redmaskdata = (uchar*)CM.m_imgRedMask.data;
			for (int j = 0; j<points[i].size(); j++)
			if (redmaskdata[points[i][j].y*CM.m_imgRedMask.cols + points[i][j].x] != 0)
				cntred++;


			if ((float)rec.width / (float)rec.height<1.3 && (float)rec.width / (float)rec.height>0.8&&rec.area()<m_ObjSize.m_maxRectAreaRED &&rec.area()>m_ObjSize.m_minRectAreaRED && (float)count / (float)rec.area()<0.9 && (float)cntred / points[i].size()>0.2 /*&& (float)cntgreen / (float)points[i].size()<0.2*/ && (float)cntadapt / (float)points[i].size()>0.3)
			//if ((float)rec.width / (float)rec.height<1.3 && (float)rec.width / (float)rec.height>0.8&&rec.area()<m_ObjSize.m_maxRectAreaRED &&rec.area()>m_ObjSize.m_minRectAreaRED)
			{
	
				// bayesian classifier
				//resize(imgTempMser, imgTempMser, Size(30, 30), 0, 0, INTER_NEAREST);
				//int bayes = BayesianClassifier(m_bayesian, imgTempMser);


				//svm classifier
				resize(imgTempMser, imgTempMser, Size(32, 32),0 ,0, INTER_CUBIC);
				int svm = SVMClassifier(m_svm, imgTempMser);



				//if (bayes == 1) //circle
				//if (svm == 5) //circle
				if (svm == 1) //circle
				{
					/*cout << "rec pos X:" << rec.x << " rec pos Y:" << rec.y<<endl;*/
					rectangle(m_imgdrawTest, Point(rec.x, rec.y), Point(rec.x + rec.width, rec.y + rec.height), CV_RGB(255, 0, 0), 2);
					vecRect.push_back(rec);
				}

				//if (svm == 6) // triangle
				//if (bayes == 2) //triangle
				if (svm == 0) // triangle
				{
					/*cout << "rec pos X:" << rec.x << " rec pos Y:" << rec.y<<endl;*/
					rectangle(m_imgdrawTest, Point(rec.x, rec.y), Point(rec.br().x, rec.br().y), CV_RGB(0, 0, 255), 2);
					vecRect.push_back(rec);
				}


			}
		}
		////////////////////////////////
		// blue sign
		if (category == bluesign || category == allsign){

			int count2 = 0;
			for (int j = 0; j<points[i].size(); j++)
			if (CM.m_imgBlueMask.at<uchar>(points[i][j]) != 0)
				count2++;

			if ((float)rec.width / (float)rec.height<1.2 && (float)rec.width / (float)rec.height>0.5 && rec.area()<m_ObjSize.m_maxRectAreaBLUE && rec.area()>m_ObjSize.m_minRectAreaBLUE /*&& (float)count2 / points[i].size()>0.7*/ && (float)count2 / points[i].size()<1 /*&& (float)cntgreen / points[i].size()<0.1*/ && (float)cntadapt / (float)points[i].size()>0.2)
			{
				// bayesian classifier
				//resize(imgTempMser, imgTempMser, Size(30, 30), 0, 0, INTER_NEAREST);
				//int bayes = BayesianClassifier(m_bayesian, imgTempMser);

				//svm classifier
				resize(imgTempMser, imgTempMser, Size(32, 32), 0, 0, INTER_CUBIC);
				int svm = SVMClassifier(m_svm, imgTempMser);


				// bayes ==0 : rectangle, bayes == 1 : circle, bayes == 2 : triangle
				
				//if (bayes<3)
				//if (svm==4 || svm==5) ///rectangle, circle
				if (svm <3) ///rectangle, circle,trianlge
				{
					rectangle(m_imgdrawTest, Point(rec.x, rec.y), Point(rec.x + rec.width, rec.y + rec.height), CV_RGB(255, 0, 255), 1);
					vecRect.push_back(rec);
				}

			}
		}


	}
	//imshow("m_imgdrawTest", m_imgdrawTest);
	t = (double)getTickCount() - t;

	points.clear();
}

void CTSD::rotate(Mat& src, double angle, Mat& dst)
{
	int len = max(src.cols, src.rows);
	cv::Point2f pt(len / 2., len / 2.);
	cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);

	cv::warpAffine(src, dst, r, cv::Size(len, len));
}

void CTSD::HistVec(Mat& imghistor, Mat &matHist)
{
	Mat dst;
	Mat imghist;
	imghistor.copyTo(imghist);
	imghist.setTo(0);
	imghistor.copyTo(dst);
	vector<int> vecHist;
	for (int i = 2; i < dst.cols; i = i + 5)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back(j);
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back(j);
			}
		}

	}

	rotate(imghistor, -90, dst);
	for (int i = 2; i < dst.cols; i = i + 5)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back(j);
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back(j);
			}
		}

	}
	rotate(dst, -90, dst);
	for (int i = 2; i < dst.cols; i = i + 5)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back(j);
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back(j);
			}
		}

	}
	rotate(dst, -90, dst);
	for (int i = 2; i < dst.cols; i = i + 5)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back(j);
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back(j);
			}
		}
	}
	
	Mat sample(1, vecHist.size(), CV_32FC1);
	for (int i = 0; i < vecHist.size(); i++)
		sample.at<float>(i) = vecHist[i];

	matHist = sample;

}

int CTSD::BayesianClassifier(CvNormalBayesClassifier *bayes, Mat & imghistor)
{
	Mat matHist;
	HistVec(imghistor, matHist);
	matHist.convertTo(matHist, CV_32FC1);
	Mat resultt;
	int response = bayes->predict(matHist, &resultt);
	//cout << resultt << endl;
	return response;
}

int CTSD::SVMClassifier(CvSVM* svm, Mat& imghistor)
{
	Mat PN_Descriptor_mtx;
	HOG_DtB_Extractor(imghistor, PN_Descriptor_mtx);

	int result = svm->predict(PN_Descriptor_mtx);
	
	return result;
}

void CTSD::HOG_DtB_Extractor(Mat &x, Mat& PN_Descriptor_mtx)
{

	/// extract feature  
	HOGDescriptor d(Size(32, 32), Size(8, 8), Size(2, 2), Size(4, 4), 9);
	vector< float> descriptorsValues;
	vector< Point> locations;
	d.compute(x, descriptorsValues, Size(0, 0), Size(0, 0), locations);
	HistVec(x, descriptorsValues);
	/// printf("descriptor number =%d\n", descriptorsValues.size() );  

	int pCol = descriptorsValues.size();
	PN_Descriptor_mtx = Mat(1, pCol, CV_32FC1);

	memcpy(&(PN_Descriptor_mtx.data[0]), descriptorsValues.data(), pCol*sizeof(float));


}


void CTSD::HistVec(Mat& imghistor, vector<float>& vecHist)
{
	Mat dst;
	Mat imghist(150, 150, CV_8UC1);
	//imghistor.copyTo(imghist);
	imghist.setTo(0);
	imghistor.copyTo(dst);

	//cout << "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
	for (int i = 1; i<dst.cols; i = i + 2)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
			}
		}

	}

	//	imshow("imghistor",imghistor);
	//waitKey(0);
	rotate(imghistor, -90, dst);
	//cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
	for (int i = 1; i<dst.cols; i = i + 2)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
			}
		}

	}

	//	imshow("imghistor90",dst);
	//waitKey(0);
	rotate(dst, -90, dst);
	//	cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
	for (int i = 1; i<dst.cols; i = i + 2)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back((float)j / (float)dst.cols);
				///				cout <<j<<" ";
			}
		}

	}

	//	imshow("imghistor180",dst);
	//waitKey(0);
	rotate(dst, -90, dst);
	//cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
	for (int i = 1; i<dst.cols; i = i + 2)
	{
		for (int j = 0; j < dst.rows; j++)
		{
			if (dst.at<uchar>(j, i) != 0)
			{
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";
				break;
			}
			else if (j == dst.rows - 1){
				vecHist.push_back((float)j / (float)dst.cols);
				//				cout <<j<<" ";

			}
		}

	}
	//	imshow("imghistor270",dst);
	//waitKey(0);

	//	cout <<"imghist.cols : " <<imghist.cols<<endl;
	//for (int i = 0; i<vecHist.size(); i++)
	//{
	//	for (int j = imghist.rows - 1; j >= 0; j--)
	//	{
	//		imghist.at<uchar>(j, i) = 255;
	//		if ((imghist.rows - 1 - j) == vecHist[i])
	//		{
	//			//cout <<i<<" "<< (imghist.rows-1-j)<<" ";
	//			break;
	//		}
	//	}
	//}

	//VecHist.push_back(vecHist);
	//cout <<endl;
	//rotate(imghist, 180, imghist);
	//	imshow("imghist",imghist);
	//	waitKey(1);
	//cout <<endl<< "vecHist size : "<<vecHist.size()<<endl;;
}