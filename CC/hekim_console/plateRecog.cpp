#include "plateRecog.h"

//
//bool LoadVideoFrame(Mat& imgInput){
//	m_vCapVideo.read(imgInput);
//	if (!imgInput.data){
//		printf("End of the video file\n");
//		return false;
//	}
//	return true;
//}
//
//void SetVideoFile(const string& strVideoPath){
//	m_vCapVideo.open(/*"sample/back.mp4"*/strVideoPath);
//	if (!m_vCapVideo.isOpened()){
//		printf("ERROR: Could not open the video file!\n");
//		exit(-1);
//	}
//}

void ClusterRectangle(vector<Rect>& vec_rectInput){

	Rect rectOverlap;
	float fOverlapRate;
	double dXDiff, dYDiff, dDiff, dComp;

	// 오버랩 비율 및 박스들 사이 거리 계산 후 군집화 수행
	for (int i = 0; i < vec_rectInput.size(); i++){
		for (int j = i + 1; j < vec_rectInput.size(); j++){
			rectOverlap = (vec_rectInput[i]) & (vec_rectInput[j]);
			fOverlapRate = rectOverlap.area() * 2 / (float)(vec_rectInput[i].area() + vec_rectInput[j].area() - rectOverlap.area());

			//point distance
			dXDiff = vec_rectInput[i].x - vec_rectInput[j].x;
			dYDiff = vec_rectInput[i].y - vec_rectInput[j].y;
			dDiff = sqrt(pow(dXDiff, 2) + pow(dYDiff, 2));
			dComp = sqrt(pow((vec_rectInput[i].width + vec_rectInput[j].width) / 2, 2) + pow((vec_rectInput[i].height + vec_rectInput[j].height) / 2, 2));

			if (fOverlapRate > 0.2&& dDiff <= dComp){
				vec_rectInput[i] = vec_rectInput[j];
				vec_rectInput.erase(vec_rectInput.begin() + j);
				j--;
			}
		}
	}
}

void GenerateCandidate(Mat& imgGray, vector<Rect>& vec_rectCandidate){
	Mat imgHist, imgEdge, imgGrayShow;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	imgGrayShow = imgGray.clone();

	//blur(imgGray, imgGray,Size(2,2));
	equalizeHist(imgGray, imgGray);
	//imshow("imgHist",imgHist);

	Canny(imgGray, imgEdge, 80, 100, 3);

	//int erosion_type = 0;
	//int erosion_size = 1;
	//Mat element = getStructuringElement(erosion_type,
	//	Size(2 * erosion_size + 1, 2 * erosion_size + 1),
	//	Point(erosion_size, erosion_size));
	//dilate(imgEdge, imgEdge, element);

	findContours(imgEdge, contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point());
	vector<vector<Point>> contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Rect> boundRect2;

	for (int i = 0; i < contours.size(); i++){
		approxPolyDP(Mat(contours[i]), contours_poly[i], 1, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
	}

	for (int i = 0; i < contours.size(); i++){
		float ratio = (double)boundRect[i].width / boundRect[i].height;

		if ((ratio >= 0.2) && (ratio <= 1.0) && (boundRect[i].area() >= 250) && (boundRect[i].area() <= 1800) && (boundRect[i].y>(imgGray.rows*3/4))
			&& (boundRect[i].x >= imgGray.cols / 3) && (boundRect[i].x <= imgGray.cols*2 / 3)){
			drawContours(imgGray, contours, i, Scalar(0, 0, 255), 1, 8, hierarchy, 0, Point());
			rectangle(imgGrayShow, boundRect[i].tl(), boundRect[i].br(), Scalar(255, 0, 0), 1, 8, 0);
			//imshow("imgGrayShow", imgGrayShow);
			boundRect2.push_back(boundRect[i]);
		}
	}
	ClusterRectangle(boundRect2);

	vec_rectCandidate = boundRect2;

	/*imshow*/("edge", imgEdge);
}

void ClusterPlateRegion(vector<Rect>& vec_rectCandidate, Rect& ROI, vector<Rect>& vec_rectPlateRegion){

	for (int k = 0; k < vec_rectCandidate.size(); k++){
		int nMinX = 999; int nMaxX = 0; int nWidth = vec_rectCandidate[k].width;
		vec_rectPlateRegion.clear();
		map<int, int> map_contour;
		map<int, int>::iterator map_iter, map_iter2;

		for (int t = k; t < vec_rectCandidate.size(); t++){
			int ndiffY = vec_rectCandidate[k].y - vec_rectCandidate[t].y;
			int ndiffHeight = vec_rectCandidate[k].height - vec_rectCandidate[t].height;

			if ((ndiffY <= 5) && (ndiffHeight <= 5)){
				map_contour[vec_rectCandidate[t].x] = vec_rectPlateRegion.size();
				vec_rectPlateRegion.push_back(vec_rectCandidate[t]);
			}
		}
		if (vec_rectPlateRegion.size() < 4) continue;
		//cout << "map_contour.size() " << map_contour.size() << endl;
		//cout << "vec_rectPlateRegion.size() " << vec_rectPlateRegion.size() << endl;

		for (int ii = 0; ii < vec_rectPlateRegion.size(); ii++){
			int nDist = 0;
			for (int jj = 0; jj < vec_rectPlateRegion.size(); jj++){
				nDist += abs(vec_rectPlateRegion[ii].x - vec_rectPlateRegion[jj].x);
			}
			nDist /= vec_rectPlateRegion.size();

			if (nDist > 4*nWidth){
				vec_rectPlateRegion.erase(vec_rectPlateRegion.begin() + ii);
				ii--;
			}
		}


		//for (map_iter = map_contour.begin(); map_iter != map_contour.end(); map_iter++){

		//	//if ((map_iter->second) == (map_contour.end()->second)) break;

		//	map_iter2 = map_iter;
		//	map_iter2++;
		//	int nDiff = abs(map_iter2->first - map_iter->first);

		//	if ((nDiff < nWidth) || (nDiff > 2*nWidth)){
		//		//cout << "before erase" << endl;
		//		vec_rectPlateRegion.erase(vec_rectPlateRegion.begin() + map_iter->second);
		//		//cout << "after erase" << endl;
		//	}
		//}
		//nMinX = map_contour.begin()->first;
		//nMaxX = map_contour.end()->first;

		for (int s = 0; s < vec_rectPlateRegion.size();s++){
			if (vec_rectPlateRegion[s].x < nMinX) nMinX = vec_rectPlateRegion[s].x;
			if (vec_rectPlateRegion[s].br().x > nMaxX) nMaxX = vec_rectPlateRegion[s].br().x;
		}


		if ((nMaxX - nMinX) > nWidth * 8){
			ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, vec_rectCandidate[k].height);
			//ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, max(35, vec_rectCandidate[k].height));
			break;
		}
	}

	//for (int k = 0; k < vec_rectCandidate.size(); k++){
	//	int nMinX = 999; int nMaxX = 0; 
	//	int nWidth = vec_rectCandidate[k].width;

	//	vec_rectPlateRegion.clear();
	//	vector<Rect> vec_rectPlateRegion2;

	//	map<int, int> map_contour;
	//	map<int, int>::iterator map_iter, map_iter2;

	//	for (int t = k; t < vec_rectCandidate.size(); t++){
	//		int ndiffX = abs(vec_rectCandidate[k].x - vec_rectCandidate[t].x);
	//		int ndiffY = abs(vec_rectCandidate[k].y - vec_rectCandidate[t].y);
	//		int ndiffHeight = abs(vec_rectCandidate[k].height - vec_rectCandidate[t].height);
	//		
	//		if ((ndiffY <= 5) && (ndiffHeight <= 5) /*&& (ndiffX <= vec_rectCandidate[k].width*8)*/){
	//			vec_rectPlateRegion.push_back(vec_rectCandidate[t]);
	//			//map_contour[vec_rectCandidate[t].x] = vec_rectPlateRegion.size();
	//			////check
	//			//if (vec_rectCandidate[t].x < nMinX) nMinX = vec_rectCandidate[t].x;
	//			//if (vec_rectCandidate[t].br().x > nMaxX) nMaxX = vec_rectCandidate[t].br().x;
	//		}
	//	}
	//	if (vec_rectPlateRegion.size() < 4) continue;
	//	
	//	for (int tt = 0; tt < vec_rectPlateRegion.size(); tt++){
	//		if (vec_rectCandidate[tt].x < nMinX) nMinX = vec_rectCandidate[tt].x;
	//		if (vec_rectCandidate[tt].br().x > nMaxX) nMaxX = vec_rectCandidate[tt].br().x;

	//		for (int pp = tt; pp < vec_rectPlateRegion.size(); pp++){				
	//			int nDiff = abs(vec_rectPlateRegion[tt].x - vec_rectPlateRegion[pp].x);
	//			if (nDiff > nWidth * 2){
	//				vec_rectPlateRegion.erase(vec_rectPlateRegion.begin()+pp);
	//				pp--;
	//			}
	//		}
	//	}

	//	if (abs(nMaxX - nMinX) < nWidth * 8){
	//		ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, vec_rectCandidate[k].height);
	//		break;
	//	}
	//	
	//	//for (map_iter = map_contour.begin(); map_iter != map_contour.end(); map_iter++){
	//	//	map_iter2 = map_iter++;
	//	//	int nInterval = abs(map_iter->first - map_iter2->first);
	//	//	if (nInterval < vec_rectCandidate[k].width){
	//	//		vec_rectPlateRegion2.push_back(vec_rectPlateRegion[map_iter->second]);
	//	//	}
	//	//	//else{
	//	//	//	if (map_iter)
	//	//	//}
	//	//}
	//	//
	//	//if ((vec_rectPlateRegion2.size()>0)&&((vec_rectPlateRegion2[0].x - vec_rectPlateRegion2.back().br().x) > nWidth * 8)){
	//	//	ROI = Rect(vec_rectPlateRegion2[0].x, vec_rectCandidate[k].y, vec_rectPlateRegion2.back().br().x - vec_rectPlateRegion2[0].x, vec_rectCandidate[k].height);
	//	//	vec_rectPlateRegion.clear();
	//	//	vec_rectPlateRegion = vec_rectPlateRegion2;
	//	//	
	//	//	//ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, max(35, vec_rectCandidate[k].height));
	//	//	break;
	//	//}
	//}

}

void RecognizePlateNumber(Mat& imgGray, Rect& ROI, vector<Rect>& vec_rectPlateRegion, vector<int>& vec_nPlateNumRst, vector<double>& vec_dPlateNumScore){

	map<int, int> plateNumRst;
	map<int, int> plateNumScore;

	int mairgin = 2;
	
	//Mat imgPlate = Mat(Size(TRAIN_SIZE * 4, TRAIN_SIZE), CV_8UC1, 0);

	for (int j = 0; j < vec_rectPlateRegion.size(); j++){
		char strName[50];
		sprintf(strName, "imgPlate %d", j);
		
		if (vec_rectPlateRegion[j].x < (ROI.x + ROI.width*0.4)) continue;
		vector<Mat> vecCNN;

		Mat imgNum = imgGray(Rect(vec_rectPlateRegion[j].x - mairgin,  vec_rectPlateRegion[j].y - mairgin, vec_rectPlateRegion[j].width + mairgin * 2, vec_rectPlateRegion[j].height + mairgin*2));
		
		/*imshow*/(strName, imgNum);

		resize(imgNum, imgNum, Size(TRAIN_SIZE, TRAIN_SIZE), 0, 0, INTER_CUBIC);
		//imshow("imgNum", imgNum);

		//imgNum.copyTo(imgPlate(Rect(j*TRAIN_SIZE, 0, TRAIN_SIZE, TRAIN_SIZE)));
		imgNum.convertTo(imgNum, CV_64FC1, 1.0 / 255, 0);
		vecCNN.push_back((imgNum - 0.5));

		Mat results, imgScore;
		cnnScore(vecCNN, results, imgScore);
		results.convertTo(results, CV_32SC1);

		int resultnum = results.at<int>(0);
		if (resultnum == 2) resultnum = 9;
		double resultScore = imgScore.at<double>(0);

		//char str[10];
		//sprintf(str, "%d", resultnum);
		//putText(imgDst, str, vec_rectPlateRegion[j].tl(), FONT_HERSHEY_PLAIN, 2, Scalar(0, 0, 255, 255));

		plateNumRst[vec_rectPlateRegion[j].tl().x] = resultnum;
		plateNumScore[vec_rectPlateRegion[j].tl().x] = resultScore;

		imgNum.release();
	}

	//imshow("imgPlate", imgPlate);

	map<int, int>::iterator map_iter, map_iter2;
	for (map_iter = plateNumRst.begin(); map_iter != plateNumRst.end(); map_iter++){
		vec_nPlateNumRst.push_back(map_iter->second);
		//vec_dPlateNumScore.push_back(map_iter->second);
	}
	for (map_iter2 = plateNumScore.begin(); map_iter2 != plateNumScore.end(); map_iter2++){
		vec_dPlateNumScore.push_back(map_iter2->second);
		//vec_dPlateNumScore.push_back(map_iter->second);
	}

	if (vec_nPlateNumRst.size() != 4){
		vec_nPlateNumRst.clear();
		vec_dPlateNumScore.clear();
	}
}

void VisualizeRegion(Mat& imgSrc, vector<Rect> vec_rectRegion, Rect& ROI, vector<int>& vec_nResult, vector<double>& vec_nPlateNumScore){

	//rectangle(imgSrc, ROI, Scalar(0, 255, 255), 2);

	//for (int i = 0; i < vec_rectRegion.size(); i++){
	//	rectangle(imgSrc, vec_rectRegion[i].tl(), vec_rectRegion[i].br(), Scalar(255, 0, 0), 2);
	//}
	//cout <<"<Label>"<< endl;
	for (int i = 0; i < vec_nResult.size(); i++){
		cout << vec_nResult[i] << " ";
	}
	cout << endl;
}

void ReturnPlateNumStr(vector<int>& vec_nResult, char* strPlateNum){
	if (vec_nResult.size() != 4) return;

	sprintf(strPlateNum, "%d%d%d%d", vec_nResult[0], vec_nResult[1], vec_nResult[2], vec_nResult[3]);
	//printf("strPlateNum %s\n", strPlateNum);
	
}

void PlateNumberRecognition(Mat& imgSrc, char* strPlateNum){
	Mat imgGray, imgDst;

	vector<Rect> vec_rectCandidate;
	Rect ROI;
	vector<Rect> vec_rectPlateRegion;
	vector<int> vec_nPlateNumRst;
	vector<double> vec_nPlateNumScore;

	Mat imgAdapt;
	float fResizeRatio = 640 / (float)imgSrc.cols;
	resize(imgSrc, imgSrc, Size(imgSrc.cols*fResizeRatio, imgSrc.rows*fResizeRatio));

	cvtColor(imgSrc, imgGray, CV_BGR2GRAY);

	imgDst = imgSrc.clone();

	GenerateCandidate(imgGray, vec_rectCandidate);
	//cout << "check" << endl;
	ClusterPlateRegion(vec_rectCandidate, ROI, vec_rectPlateRegion);
	//cout << "check" << endl;
	adaptiveThreshold(imgGray, imgAdapt, 250, ADAPTIVE_THRESH_MEAN_C, // Adaptive 함수
		THRESH_BINARY, // 이진화 타입
		/*blockSize*/11,  // 이웃크기
		/*threshold*/10); // threshold used

	imgAdapt = abs(imgAdapt - 255);

	RecognizePlateNumber(imgAdapt, ROI, vec_rectPlateRegion, vec_nPlateNumRst, vec_nPlateNumScore);
	//VisualizeRegion(imgDst, vec_rectPlateRegion, ROI, vec_nPlateNumRst, vec_nPlateNumScore);
	ReturnPlateNumStr(vec_nPlateNumRst, strPlateNum);


}