#include "TSC.h"
#include "TST.h"

CTSC::CTSC()
{
	m_MatAverage = Mat::zeros(32, 32, CV_64FC3);
}


CTSC::~CTSC()
{
}
void CTSC::SetVanishingPT(Point pt)
{
	m_ptVanishing = pt;
}


void CTSC::Validation_classification(Mat& imgshow, Mat &srcImage, SVecMSERs& MserVec, vector<SKalman>& MultiKF, SVecTracking& High, int& cntframe, Rect& ROIset, Mat& imgROImask)
{
	
	// High.vecBefore : tracking 된 target rect
	// MserVec.m_vecRectMser : 모든 segmentation된 rect

	vector<Rect> vecDetect;
	Point pCurrent;
	vector<Rect> vecValid;
	
	vecValid = High.vecBefore; 

	double time5 = (double)getTickCount();
	int szLager = 10;
	for (int i = 0; i<vecValid.size(); i++)
	{
		if (vecValid[i].x<szLager || vecValid[i].x>srcImage.cols - szLager || vecValid[i].y<szLager || vecValid[i].y> srcImage.rows - szLager)
			continue;
		int TempArea = vecValid[i].area();
		Rect recOri = vecValid[i];

		vecValid[i].x -= szLager;
		vecValid[i].y -= szLager;
		vecValid[i].width += szLager * 2;
		vecValid[i].height += szLager * 2;
		cout << "vecValid[i].height: "<<vecValid[i].height <<" High.vecBefore[i].height: "<<High.vecBefore[i].height<<endl;
		int margin = 3;
		if (imgROImask.at<uchar>((vecValid[i].y - margin < 0 ? 0 : vecValid[i].y - margin), (vecValid[i].x - margin <0 ? 0 : vecValid[i].x - margin)) == 0 || imgROImask.at<uchar>((vecValid[i].y + vecValid[i].height - margin > srcImage.rows - 1 ? srcImage.rows : vecValid[i].y + vecValid[i].height - margin), (vecValid[i].x + vecValid[i].width - margin > srcImage.cols ? (srcImage.cols - 1) : (vecValid[i].x + vecValid[i].width - margin))) == 0)
			continue;

		cout << "MserVec.m_vecRectMser.size() : " << MserVec.m_vecRectMser.size() << endl;

		Rect recValid;
		Rect recDetect;
		float overlap = 0;

		for (int j = 0; j<MserVec.m_vecRectMser.size(); j++)
		{
			recValid = vecValid[i] & MserVec.m_vecRectMser[j];
			float overlapRate = (float)recValid.area() / (float)vecValid[i].area();
			if (overlap < overlapRate && MserVec.m_vecRectMser[j].x > vecValid[i].x&& MserVec.m_vecRectMser[j].y > vecValid[i].y && MserVec.m_vecRectMser[j].x + MserVec.m_vecRectMser[j].width < vecValid[i].x + vecValid[i].width&& MserVec.m_vecRectMser[j].y + MserVec.m_vecRectMser[j].height < vecValid[i].y + vecValid[i].height)
			{
				if (((float)MserVec.m_vecRectMser[j].width / (float)MserVec.m_vecRectMser[j].height>0.8 && (float)MserVec.m_vecRectMser[j].width / (float)MserVec.m_vecRectMser[j].height<1.2) || ((float)MserVec.m_vecRectMser[j].width / (float)MserVec.m_vecRectMser[j].height>1.5 && (float)MserVec.m_vecRectMser[j].width / (float)MserVec.m_vecRectMser[j].height<6))
				{
					overlap = overlapRate;
					recDetect = MserVec.m_vecRectMser[j];
				}
			}
		}


		//재조정된 rect에 대해서 분류
		if (recDetect.width != 0)
		{
			if ((float)recDetect.area()*1.2>TempArea && (float)recDetect.area()*0.8<TempArea)
			{
				rectangle(imgshow, recDetect, MultiKF[i].rgb, 2);
				Point ptCenter = Point(recDetect.x + recDetect.width / 2, recDetect.y + recDetect.height / 2);
				cout << "ptCenter : " << ptCenter << endl;
				drawCross(imgshow, ptCenter, MultiKF[i].rgb, 5);
				line(imgshow, m_ptVanishing, ptCenter, MultiKF[i].rgb, 2);
				CNN(imgshow, srcImage, recDetect);
				cout << m_ptVanishing << endl;

				float fDistXbefore = m_ptVanishing.x - ptCenter.x;
				float fDistYbefore = m_ptVanishing.y - ptCenter.y;

				float fAngleend = fAngle(fDistXbefore, fDistYbefore)-90;
				fAngleend < 0 ? fAngleend += 360 : fAngleend;
				char szAngle[50];
				sprintf(szAngle, "%.1f", fAngleend);

				putText(imgshow, szAngle, Point((m_ptVanishing.x *(i + 1) / (2 + i) + ptCenter.x / (2 + i)), (m_ptVanishing.y *(i + 1) / (2 + i) + ptCenter.y / (2 + i))), FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(MultiKF[i].rgb.val[2] * 2 / 3, MultiKF[i].rgb.val[1] * 2 / 3, MultiKF[i].rgb.val[0] * 2 / 3), 2);

			}
			else
			{
				
				rectangle(imgshow, recOri, MultiKF[i].rgb, 2);
				Point ptCenter = Point(recOri.x + recOri.width / 2, recOri.y + recOri.height / 2);
				cout << "ptCenter : " << ptCenter << endl;
				drawCross(imgshow, ptCenter, MultiKF[i].rgb, 5);
				cout << m_ptVanishing << endl;
				line(imgshow, m_ptVanishing, ptCenter, MultiKF[i].rgb, 2);
				CNN(imgshow, srcImage, recOri);

				float fDistXbefore = m_ptVanishing.x - ptCenter.x;
				float fDistYbefore = m_ptVanishing.y - ptCenter.y;

				float fAngleend = fAngle(fDistXbefore, fDistYbefore) - 90;
				fAngleend < 0 ? fAngleend += 360 : fAngleend;
				char szAngle[50];
				sprintf(szAngle, "%.1f", fAngleend);

				putText(imgshow, szAngle, Point((m_ptVanishing.x *(i + 1) / (2 + i) + ptCenter.x / (2 + i)), (m_ptVanishing.y *(i + 1) / (2 + i) + ptCenter.y / (2 + i))), FONT_HERSHEY_SIMPLEX, 0.5, CV_RGB(MultiKF[i].rgb.val[2] * 2 / 3, MultiKF[i].rgb.val[1] * 2 / 3, MultiKF[i].rgb.val[0] * 2 / 3), 2);

				rectangle(srcImage, vecValid[i], MultiKF[i].rgb, 2);
	
			}

		}

		overlap = 0;
	}




	time5 = (double)getTickCount() - time5;
	printf("validation : %f ms.\n", time5*1000. / getTickFrequency());

	double cucnn = time5*1000. / getTickFrequency();
	//avg_cnn += cucnn;

	//cntcnn++;

	//printf("Avg cnn: %f ms.\n", avg_cnn / cntcnn);
	MserVec.m_vecPoints.clear();
	MserVec.m_vecRectMser.clear();
	MserVec.m_vecImgMser.clear();
	//vecRectTracking.clear();
}

void CTSC::CNN(Mat& imgshow, Mat& imgsrc, Rect & rec)
{
		char roiName[100] = { 0 };
		Mat img = imgsrc(rec).clone();

		cvtColor(img, img, CV_BGR2GRAY);

		vector<Mat> vecCNN;
		//Mat imgCNN;

		equalizeHist(img, img);
		resize(img, img, Size(32, 32), 0, 0, INTER_CUBIC);
		//cvtColor(img, imgCNN, CV_RGB2GRAY);	// Cheng-Bin Jin

		//imgCNN = img;

		img.convertTo(img, CV_64FC1, 1.0 / 255, 0);
		vecCNN.push_back((img - 0.5));
		//vecCNN.push_back((imgCNN - m_MatAverage));
		

		Mat results;
		cnnMain(vecCNN, results);
		results.convertTo(results, CV_32SC1);
//		cout << "result : " << results.at<int>(0) << endl;
		int resultnum = results.at<int>(0);
		if (resultnum > -1 && resultnum <19)
		{
//			cout << "resultnum : " << resultnum << endl;
			sprintf(roiName, "popup/show%d.png", resultnum+1);
//			cout << roiName << endl;
	
			Mat imgPopup = imread(roiName, 1);
			//Mat imgPopup = Mat::zeros(100,100,CV_8UC3);
			//imshow("pop", imgPopup);
			resize(imgPopup, imgPopup, rec.size());
			Rect poprec;
			poprec.x = rec.x;
			poprec.y = rec.y + rec.height + 1;
			poprec.width = rec.width;
			poprec.height = rec.height;
			//imgsrc(poprec).cop
			imgPopup.copyTo(imgshow(poprec));
	
	
		}
	
	
		vecCNN.clear();
}
