#include <cv.h>
#include <highgui.h>
#include <iostream>

#include "TSD.h"
#include "TST.h"
#include "TSC.h"

#define TRAIN_SIZE 32

using namespace cv;
using namespace std;

SRecSize recSZ;

float fscale = 1.0f;

CvSVM* m_svm = new CvSVM;
SVecMSERs MserVec;

//void FindTailLight(Mat& imgSrc, vector<Point>& vec_ptLight);
//void enhancement(Mat imgR, Mat imgG, Mat imgB, Mat& imgDst, Mat& imgDst2);
//void Acromatic(Mat& imgRGB, Mat& imgMask, float fvalue, uchar uThreshold);
//void ColorMask(SImgMask& CM, Mat& imgHue, Mat& imgRGB);
//int SVMClassifier(CvSVM* svm, Mat& imghistor, double& score);
//void rotate(cv::Mat& src, double angle, cv::Mat& dst);
//void HistVec(Mat& imghistor, vector<float>& vecHist);
//void HOG_DtB_Extractor(Mat &x, Mat& PN_Descriptor_mtx);
//void MSERsegmentation(MSER& mser, Mat& imgInputMSER, SImgMask& CM, vector<Point>& vec_ptLight, vector<vector<Point>>& points, SImgAcromatic& AM);
void ClusterRectangle(vector<Rect>& vec_rectInput);
void GenerateCandidate(Mat& imgSrc, vector<Rect>& vec_rectCandidate);
void ClusterPlateRegion(vector<Rect>& vec_rectCandidate, Rect& ROI, vector<Rect>& vec_rectPlateRegion);
void RecognizePlateNumber(Mat& imgGray, Rect& ROI, vector<Rect>& vec_rectPlateRegion, vector<int>& vec_nPlateNumRst);

VideoCapture				m_vCapVideo;
Size2i						sizeInputVideo;

bool LoadVideoFrame(Mat& imgInput){
	m_vCapVideo.read(imgInput);
	if (!imgInput.data){
		printf("End of the video file\n");
		return false;
	}
	return true;
}

void SetVideoFile(const string& strVideoPath){
	m_vCapVideo.open(/*"sample/back.mp4"*/strVideoPath);
	if (!m_vCapVideo.isOpened()){
		printf("ERROR: Could not open the video file!\n");
		exit(-1);
	}
}


//
//void enhancement(Mat imgR, Mat imgG, Mat imgB, Mat& imgDst, Mat& imgDst2)
//{
//	imgR.convertTo(imgR, CV_32FC1);
//	imgG.convertTo(imgG, CV_32FC1);
//	imgB.convertTo(imgB, CV_32FC1);
//	Mat imgTempR(imgDst.size(), CV_32FC1);
//	Mat imgTempB(imgDst.size(), CV_32FC1);
//
//	for (int i = 0; i < imgDst.cols; i++)
//	{
//		for (int j = 0; j < imgDst.rows; j++)
//		{
//			imgTempR.at<float>(j, i) = 255 * 2 * max(0, max(imgR.at<float>(j, i) - imgG.at<float>(j, i), imgR.at<float>(j, i) - imgB.at<float>(j, i)) / (imgR.at<float>(j, i) + imgG.at<float>(j, i) + imgB.at<float>(j, i)));
//			imgTempB.at<float>(j, i) = 255 * 2 * max(0, max(imgB.at<float>(j, i) - imgG.at<float>(j, i), imgB.at<float>(j, i) - imgR.at<float>(j, i)) / (imgR.at<float>(j, i) + imgG.at<float>(j, i) + imgB.at<float>(j, i)));
//		}
//
//	}
//
//	imgTempR.convertTo(imgDst, CV_8UC1);
//	imgTempB.convertTo(imgDst2, CV_8UC1);
//
//}
//void Acromatic(Mat& imgRGB, Mat& imgMask, float fvalue, uchar uThreshold)
//{
//	float facromatic = 0;
//	vector<Mat> vecRGB;
//	split(imgRGB, vecRGB);
//	int cols = imgRGB.cols;
//	for (int i = 0; i < imgRGB.rows; i++)
//	{
//		for (int j = 0; j < imgRGB.cols; j++)
//		{
//			float sum = (abs((float)vecRGB[0].at<uchar>(i*cols + j) - (float)vecRGB[1].at<uchar>(i*cols + j)) + abs((float)vecRGB[0].at<uchar>(i*cols + j) - (float)vecRGB[2].at<uchar>(i*cols + j)) + abs((float)vecRGB[1].at<uchar>(i*cols + j) - (float)vecRGB[2].at<uchar>(i*cols + j)));
//			facromatic = sum / (3 * fvalue);
//			if (facromatic < 1)
//				imgMask.at<uchar>(i*cols + j) = 0;
//			else if (facromatic >= 1 || (vecRGB[0].at<uchar>(i*cols + j) < uThreshold&&vecRGB[1].at<uchar>(i*cols + j) < uThreshold&&vecRGB[2].at<uchar>(i*cols + j) < uThreshold))
//				imgMask.at<uchar>(i*cols + j) = 255;
//
//		}
//	}
//
//
//}
//void ColorMask(SImgMask& CM, Mat& imgHue, Mat& imgRGB)
//{
//	vector<Mat> vecRGB;
//	split(imgRGB, vecRGB);
//
//	//cout << vecHSV[0]<<endl;
//	for (int i = 0; i < imgHue.rows*imgHue.cols; i++)
//	{
//		if (((imgHue.at<uchar>(i) >= 0 && imgHue.at<uchar>(i) < 20) || (imgHue.at<uchar>(i) > 130 && imgHue.at<uchar>(i) <= 180) || (abs(vecRGB[0].at<uchar>(i)-vecRGB[1].at<uchar>(i)) < 5 && abs(vecRGB[1].at<uchar>(i)-vecRGB[2].at<uchar>(i)) < 5 && abs(vecRGB[2].at<uchar>(i)-vecRGB[0].at<uchar>(i)) < 5)) && vecRGB[0].at<uchar>(i) < 200 && vecRGB[1].at<uchar>(i) < 200)
//			CM.m_imgRedMask.at<uchar>(i) = 255;
//		else
//			CM.m_imgRedMask.at<uchar>(i) = 0;
//
//		if (imgHue.at<uchar>(i) > 100 && imgHue.at<uchar>(i) < 125)
//			CM.m_imgBlueMask.at<uchar>(i) = 255;
//		else
//			CM.m_imgBlueMask.at<uchar>(i) = 0;
//
//		//if(imgHue.at<uchar>(i)>20 && imgHue.at<uchar>(i)<90 && (vecRGB[0].at<uchar>(i)<220 || vecRGB[1].at<uchar>(i)<220 || vecRGB[2].at<uchar>(i)<220))
//		if (imgHue.at<uchar>(i) > 20 && imgHue.at<uchar>(i) < 90 && (vecRGB[0].at<uchar>(i) < 220 && vecRGB[1].at<uchar>(i) < 230 && vecRGB[2].at<uchar>(i) < 220))
//			CM.m_imgGreenMask.at<uchar>(i) = 255;
//		else
//			CM.m_imgGreenMask.at<uchar>(i) = 0;
//
//
//
//		int absVal = abs(vecRGB[0].at<uchar>(i)-vecRGB[1].at<uchar>(i));
//
//		if (imgHue.at<uchar>(i) > 20 && imgHue.at<uchar>(i) < 90 && (vecRGB[0].at<uchar>(i) < 255 && vecRGB[1].at<uchar>(i) > 100 && vecRGB[2].at<uchar>(i) < 100) && absVal < 30)
//			CM.m_imgGreenLight.at<uchar>(i) = 255;
//		else
//			CM.m_imgGreenLight.at<uchar>(i) = 0;
//
//
//		if (vecRGB[0].at<uchar>(i) < 120 && vecRGB[1].at<uchar>(i) < 110 && vecRGB[2].at<uchar>(i) < 90 && absVal < 20)
//			CM.m_imgBlackMask.at<uchar>(i) = 255;
//		else
//			CM.m_imgBlackMask.at<uchar>(i) = 0;
//
//	}
//}
//int SVMClassifier(CvSVM* svm, Mat& imghistor, double& score)
//{
//	Mat PN_Descriptor_mtx;
//	HOG_DtB_Extractor(imghistor, PN_Descriptor_mtx);
//
//	int result = svm->predict(PN_Descriptor_mtx);
//	score = svm->predict(PN_Descriptor_mtx, true);
//
//	return result;
//}
//void rotate(cv::Mat& src, double angle, cv::Mat& dst)
//{
//
//	int len = max(src.cols, src.rows);
//	cv::Point2f pt(len / 2., len / 2.);
//	cv::Mat r = cv::getRotationMatrix2D(pt, angle, 1.0);
//
//	cv::warpAffine(src, dst, r, cv::Size(len, len));
//}
//void HistVec(Mat& imghistor, vector<float>& vecHist)
//{
//	Mat dst;
//	Mat imghist(150, 150, CV_8UC1);
//	//imghistor.copyTo(imghist);
//	imghist = 0;
//	imghistor.copyTo(dst);
//
//	//cout << "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
//	for (int i = 1; i < dst.cols; i = i + 2)
//	{
//		for (int j = 0; j < dst.rows; j++)
//		{
//			if (dst.at<uchar>(j, i) != 0)
//			{
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//				break;
//			}
//			else if (j == dst.rows - 1){
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//			}
//		}
//
//	}
//
//	//	imshow("imghistor",imghistor);
//	//waitKey(0);
//	rotate(imghistor, -90, dst);
//	//cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
//	for (int i = 1; i < dst.cols; i = i + 2)
//	{
//		for (int j = 0; j < dst.rows; j++)
//		{
//			if (dst.at<uchar>(j, i) != 0)
//			{
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//				break;
//			}
//			else if (j == dst.rows - 1){
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//			}
//		}
//
//	}
//
//	//	imshow("imghistor90",dst);
//	//waitKey(0);
//	rotate(dst, -90, dst);
//	//	cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
//	for (int i = 1; i < dst.cols; i = i + 2)
//	{
//		for (int j = 0; j < dst.rows; j++)
//		{
//			if (dst.at<uchar>(j, i) != 0)
//			{
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//				break;
//			}
//			else if (j == dst.rows - 1){
//				vecHist.push_back((float)j / (float)dst.cols);
//				///				cout <<j<<" ";
//			}
//		}
//
//	}
//
//	//	imshow("imghistor180",dst);
//	//waitKey(0);
//	rotate(dst, -90, dst);
//	//cout <<endl<< "dst.cols : "<<dst.cols <<", dst.rows : "<<dst.rows<<endl;
//	for (int i = 1; i < dst.cols; i = i + 2)
//	{
//		for (int j = 0; j < dst.rows; j++)
//		{
//			if (dst.at<uchar>(j, i) != 0)
//			{
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//				break;
//			}
//			else if (j == dst.rows - 1){
//				vecHist.push_back((float)j / (float)dst.cols);
//				//				cout <<j<<" ";
//
//			}
//		}
//
//	}
//
//}
//void HOG_DtB_Extractor(Mat &x, Mat& PN_Descriptor_mtx)
//{
//
//	/// extract feature  
//	HOGDescriptor d(Size(32, 32), Size(8, 8), Size(2, 2), Size(4, 4), 9);
//	vector< float> descriptorsValues;
//	vector< Point> locations;
//	d.compute(x, descriptorsValues, Size(0, 0), Size(0, 0), locations);
//	HistVec(x, descriptorsValues);
//	/// printf("descriptor number =%d\n", descriptorsValues.size() );  
//
//	int pCol = descriptorsValues.size();
//	PN_Descriptor_mtx = Mat(1, pCol, CV_32FC1);
//
//	memcpy(&(PN_Descriptor_mtx.data[0]), descriptorsValues.data(), pCol*sizeof(float));
//
//
//}
//void MSERsegmentation(MSER& mser, Mat& imgInputMSER, SImgMask& CM, vector<Point>& vec_ptLight, vector<vector<Point>>& points, SImgAcromatic& AM)
//{
//	mser(imgInputMSER, points);
//
//	Mat imgDrawMser;
//	Point ptLeft, ptRight;
//	ptLeft.x = 999; ptRight.x = 0;
//	ptLeft.y = 1234; ptRight.y = 1234;
//
//	//int nMinX=999, nMaxX=0;
//
//	cvtColor(imgInputMSER, imgDrawMser, CV_GRAY2BGR);
//
//	for (int i = 0; i < (int)points.size(); i++)
//	{
//		vector<Point>& r = points[i]; //
//		Rect rectRegion = boundingRect(r);
//		if (rectRegion.tl().x < ptLeft.x){
//			ptLeft = rectRegion.tl();
//		}		
//		if (rectRegion.br().x > ptRight.x){
//			ptRight = rectRegion.br();
//		}
//		
//		/*if ((float)rec.width / (float)rec.height<1.3 && (float)rec.width / (float)rec.height>0.7 &&rec.area()<recSZ.m_maxRectAreaRED &&rec.area()>recSZ.m_minRectAreaRED)
//		{*/
//		for (int j = 0; j < (int)r.size(); j++){
//			Point pt = r[j];
//			imgDrawMser.at<Vec3b>(pt) = bcolors[i % 9];
//		}
//		//}
//		r.clear();
//	}
//
//	vec_ptLight.push_back(ptRight);
//	vec_ptLight.push_back(ptLeft);
//
//	imshow("imgDrawMser", imgDrawMser);
//}

void ClusterRectangle(vector<Rect>& vec_rectInput){

	Rect rectOverlap;
	float fOverlapRate;
	double dXDiff, dYDiff, dDiff, dComp;

	// 오버랩 비율 및 박스들 사이 거리 계산 후 군집화 수행
	for (int i = 0; i < vec_rectInput.size(); i++){
		for (int j = i + 1; j < vec_rectInput.size(); j++){
			rectOverlap = (vec_rectInput[i]) & (vec_rectInput[j]);
			fOverlapRate = rectOverlap.area() * 2 / (float)(vec_rectInput[i].area() + vec_rectInput[j].area() - rectOverlap.area());

			//point distance
			dXDiff = vec_rectInput[i].x - vec_rectInput[j].x;
			dYDiff = vec_rectInput[i].y - vec_rectInput[j].y;
			dDiff = sqrt(pow(dXDiff, 2) + pow(dYDiff, 2));
			dComp = sqrt(pow((vec_rectInput[i].width + vec_rectInput[j].width) / 2, 2) + pow((vec_rectInput[i].height + vec_rectInput[j].height) / 2, 2));

			if (fOverlapRate > 0.2&& dDiff <= dComp){
				vec_rectInput[i] = vec_rectInput[j];
				vec_rectInput.erase(vec_rectInput.begin() + j);
				j--;
			}
		}
	}
}

//void FindTailLight(Mat& imgSrc, vector<Point>& vec_ptLight){
//	Mat imgHSV, imgGray;
//
//
//	recSZ.m_minRectAreaRED = 100 * fscale;
//	recSZ.m_maxRectAreaRED = 15000 * fscale;
//	recSZ.m_minRectAreaBLUE = 100 * fscale;
//	recSZ.m_maxRectAreaBLUE = 18000 * fscale;
//	recSZ.m_minRectAreaBLACK = 100 * fscale;
//	recSZ.m_maxRectAreaBLACK = 3000 * fscale;
//
//	cvtColor(imgSrc, imgGray, CV_BGR2GRAY);
//
//	vector<Mat> vecRGB;
//	split(imgSrc, vecRGB);
//
//	cvtColor(imgSrc, imgHSV, CV_BGR2HSV);
//	vector<Mat> vecHSV;
//	split(imgHSV, vecHSV);
//
//	Mat imgEnhanceR(imgSrc.size(), CV_8UC1);
//	Mat imgEnhanceB(imgSrc.size(), CV_8UC1);
//	enhancement(vecRGB[2], vecRGB[1], vecRGB[0], imgEnhanceR, imgEnhanceB);
//
//	imshow("imgEnhanceR", imgEnhanceR);
//
//	/////////////////////////////////////////////////////////////////////////////////
//
//	SImgAcromatic AM;
//	Mat imgRedMaskAM;
//
//	AM.m_imgAcroMaskBlue = Mat::zeros(imgSrc.rows, imgSrc.cols, CV_8UC1);
//	AM.m_imgAcroMaskRed = Mat::zeros(imgSrc.rows, imgSrc.cols, CV_8UC1) + 255;
//	AM.m_imgAcroMaskMSER = Mat::zeros(imgSrc.rows, imgSrc.cols, CV_8UC1);
//	AM.m_imgAcroMaskLight = Mat::zeros(imgSrc.rows, imgSrc.cols, CV_8UC1);
//
//	//		Acromatic(imgInput, imgAcroMaskMSER, 10, 30); // 무채색 지우기
//	Acromatic(imgSrc, AM.m_imgAcroMaskBlue, 10, 30);
//	Acromatic(imgSrc, AM.m_imgAcroMaskRed, 10, 30);
//	Acromatic(imgSrc, AM.m_imgAcroMaskMSER, 10, 0);
//	Acromatic(imgSrc, AM.m_imgAcroMaskLight, 30, 0);
//
//	AM.m_imgAcroMaskMSER = 255 - AM.m_imgAcroMaskMSER;
//
//	//////////////////////////  ColorMask  //////////////////////////////////////////
//	SImgMask CM;
//	CM.m_imgRedMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
//	CM.m_imgBlueMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
//	CM.m_imgGreenMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
//	CM.m_imgBlackMask = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
//	CM.m_imgGreenLight = Mat::zeros(vecHSV[0].rows, vecHSV[0].cols, CV_8UC1);
//	ColorMask(CM, vecHSV[0], imgSrc);
//
//	bitwise_and(CM.m_imgBlueMask, AM.m_imgAcroMaskBlue, CM.m_imgBlueMask);
//
//	////////////////////////////////////////////////////////////////////////////////
//	Mat imgAdapThres;
//	adaptiveThreshold(vecHSV[2], imgAdapThres, 255, CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY_INV, 31, 5);
//
//	merge(vecHSV, imgHSV);
//
//	//////MSER//////////////////////////////////////////////////////////////////////////
//	vector<vector<Point>> points;
//
//	MSER mserSaturation(7, 50, 10000, 0.2, 0.1, 250, 1.01, 0.003, 5); //
//	//MSER mserSaturation(7, 50, 10000, 0.2, 0.05, 100, 1.01, 0.003, 5); //
//	MSERsegmentation(mserSaturation, imgEnhanceR, CM, vec_ptLight, points, AM);
//
//	// found red region
//	if (vec_ptLight[0].y != 1234 && vec_ptLight[1].y != 1234){
//		vec_ptLight = vec_ptLight;
//	}
//	else{
//	// not found red region
//		return;
//	}
//
//}

void GenerateCandidate(Mat& imgGray, vector<Rect>& vec_rectCandidate){
	Mat imgHist, imgEdge;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	//blur(imgGray, imgGray,Size(2,2));
	equalizeHist(imgGray, imgHist);

	Canny(imgHist, imgEdge, 80, 100, 3);

	//int erosion_type = 0;
	//int erosion_size = 1;
	//Mat element = getStructuringElement(erosion_type,
	//	Size(2 * erosion_size + 1, 2 * erosion_size + 1),
	//	Point(erosion_size, erosion_size));
	//dilate(imgEdge, imgEdge, element);

	findContours(imgEdge, contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point());
	vector<vector<Point>> contours_poly(contours.size());
	vector<Rect> boundRect(contours.size());
	vector<Rect> boundRect2;

	for (int i = 0; i < contours.size(); i++){
		approxPolyDP(Mat(contours[i]), contours_poly[i], 1, true);
		boundRect[i] = boundingRect(Mat(contours_poly[i]));
	}

	for (int i = 0; i < contours.size(); i++){
		float ratio = (double)boundRect[i].width / boundRect[i].height;

		if ((ratio >= 0.4) && (ratio <= 1.0) && (boundRect[i].area() >= 250) && (boundRect[i].area() <= 1800)){
			drawContours(imgGray, contours, i, Scalar(0, 0, 255), 1, 8, hierarchy, 0, Point());
			rectangle(imgGray, boundRect[i].tl(), boundRect[i].br(), Scalar(255, 0, 0), 1, 8, 0);
			imshow("imgGray", imgGray);
			boundRect2.push_back(boundRect[i]);
		}
	}
	ClusterRectangle(boundRect2);

	vec_rectCandidate = boundRect2;

	imshow("edge", imgEdge);
}

void ClusterPlateRegion(vector<Rect>& vec_rectCandidate, Rect& ROI, vector<Rect>& vec_rectPlateRegion){
	
	for (int k = 0; k < vec_rectCandidate.size(); k++){
		int nMinX = 999; int nMaxX = 0; int nWidth = vec_rectCandidate[k].width;
		vec_rectPlateRegion.clear();
		for (int t = k; t < vec_rectCandidate.size(); t++){
			int ndiffY = vec_rectCandidate[k].y - vec_rectCandidate[t].y;
			int ndiffHeight = vec_rectCandidate[k].height - vec_rectCandidate[t].height;

			if ((ndiffY <= 5) && (ndiffHeight <= 5)){
				vec_rectPlateRegion.push_back(vec_rectCandidate[t]);

				if (vec_rectCandidate[t].x < nMinX) nMinX = vec_rectCandidate[t].x;
				if (vec_rectCandidate[t].br().x > nMaxX) nMaxX = vec_rectCandidate[t].br().x;
			}
		}
		if (vec_rectPlateRegion.size() < 4) continue;
		if ((nMaxX - nMinX) > nWidth * 8){
			ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, vec_rectCandidate[k].height);
			//ROI = Rect(nMinX, vec_rectCandidate[k].y, nMaxX - nMinX, max(35, vec_rectCandidate[k].height));
			break;
		}
	}

}

void RecognizePlateNumber(Mat& imgGray, Rect& ROI, vector<Rect>& vec_rectPlateRegion, vector<int>& vec_nPlateNumRst){

	map<int, int> plateNumRst;

	for (int j = 0; j < vec_rectPlateRegion.size(); j++){
		if (vec_rectPlateRegion[j].x < (ROI.x + ROI.width*0.4)) continue;
		vector<Mat> vecCNN;

		Mat imgNum = imgGray(vec_rectPlateRegion[j]);
		resize(imgNum, imgNum, Size(TRAIN_SIZE, TRAIN_SIZE), 0, 0, INTER_CUBIC);
		imgNum.convertTo(imgNum, CV_64FC1, 1.0 / 255, 0);
		vecCNN.push_back((imgNum - 0.5));

		Mat results, imgScore;
		cnnScore(vecCNN, results, imgScore);
		results.convertTo(results, CV_32SC1);

		int resultnum = results.at<int>(0);

		//char str[10];
		//sprintf(str, "%d", resultnum);
		//putText(imgDst, str, vec_rectPlateRegion[j].tl(), FONT_HERSHEY_PLAIN, 2, Scalar(0, 0, 255, 255));

		plateNumRst[vec_rectPlateRegion[j].tl().x] = resultnum;
	}

	map<int, int>::iterator map_iter;
	for (map_iter = plateNumRst.begin(); map_iter != plateNumRst.end(); map_iter++){
		vec_nPlateNumRst.push_back(map_iter->second);
	}
}

void VisualizeRegion(Mat& imgSrc, vector<Rect> vec_rectRegion, Rect& ROI, vector<int>& vec_nResult){

	rectangle(imgSrc, ROI, Scalar(0,255, 255), 2);

	for (int i = 0; i < vec_rectRegion.size();i++){
		rectangle(imgSrc, vec_rectRegion[i].tl(), vec_rectRegion[i].br(), Scalar(255, 0, 0), 2);
	}
	for (int i = 0; i < vec_nResult.size(); i++){
		cout << vec_nResult[i] << " ";
	}
	cout << endl;
}

int main()
{
	//VideoCapture cap(0);
	//if (!cap.isOpened()){
	//	cout << "[ERROR]  could not open camera" << endl;
	//	return 0;
	//}
	SetVideoFile("sample/back.mp4");

	Mat imgSrc, imgGray, imgDst;

	char Xmlfilename[100] = "train_cnn_svhn.xml";
	initRead(Xmlfilename);

	while (true){
		bool bFrameValid = true;

		if (!LoadVideoFrame(imgSrc)){
			bool bFrameValid = false;
			break;
		}


		vector<Rect> vec_rectCandidate;
		Rect ROI;
		vector<Rect> vec_rectPlateRegion;
		vector<int> vec_nPlateNumRst;

		/*bool bFrameValid = true;*/

		float fResizeRatio = 640 / (float)imgSrc.cols;
		resize(imgSrc, imgSrc, Size(imgSrc.cols*fResizeRatio, imgSrc.rows*fResizeRatio));
		
		//try{
		//	cap >> imgSrc;
			imgDst = imgSrc.clone();
			cvtColor(imgSrc, imgGray, CV_RGB2GRAY);
		//	//resize(imgSrc, imgSrc, Size(imgSrc.cols*0.2, imgSrc.rows*0.2));
		//}
		//catch (Exception& e){
		//	std::cerr << "Exception occurred. Ignoring frame... " << e.err << std::endl;
		//	bFrameValid = false;
		//}
		if (bFrameValid){
			Mat imgAdaptT, imgErod;
			//adaptiveThreshold(imgGray, imgAdaptT, 100, 250,CV_THRESH_BINARY, );
			adaptiveThreshold(imgGray, imgAdaptT, 250, ADAPTIVE_THRESH_MEAN_C, // Adaptive 함수
				THRESH_BINARY, // 이진화 타입
				/*blockSize*/11,  // 이웃크기
				/*threshold*/10); // threshold used
			imgAdaptT = abs(imgAdaptT - 255);

			//int erosion_type = 0;
			//int erosion_size = 1;
			//Mat element = getStructuringElement(erosion_type,
			//	Size(2 * erosion_size + 1, 2 * erosion_size + 1),
			//	Point(erosion_size, erosion_size));
			//erode(imgAdaptT, imgErod, element);

			GenerateCandidate(imgGray, vec_rectCandidate);
			ClusterPlateRegion(vec_rectCandidate, ROI, vec_rectPlateRegion);
			RecognizePlateNumber(imgGray, ROI, vec_rectPlateRegion, vec_nPlateNumRst);
			VisualizeRegion(imgDst, vec_rectPlateRegion, ROI, vec_nPlateNumRst);

			imshow("threshold", imgAdaptT);
			imshow("imgErod", imgErod);
			imshow("src", imgSrc);
			imshow("dst", imgDst);

			cvWaitKey(0);
		}
	}
	return 0;
}

