#include <iostream>
#include <stdio.h>
#include "opencv/cv.h"
#include "opencv/highgui.h"
#include "opencv2/opencv.hpp"

#include "TSD.h"
#include "TST.h"
#include "TSC.h"

#define TRAIN_SIZE 32
#define NUM_WIDTH 15
#define NUM_HEIGHT 25

int main(){
	char Xmlfilename[100] = "train_cnn_svhn.xml";
	initRead(Xmlfilename);

	Rect rectNumRegion;
	
	Mat img = imread("./plate_image/template/plate2.png", 1);
	cvtColor(img, img, CV_RGB2GRAY);

	float fResizeRate = TRAIN_SIZE / (float)img.rows;
	resize(img, img, Size(img.cols*fResizeRate, img.rows*fResizeRate));

	int nStartX = 0.45*img.cols;

	for (int i = 0; i < 4; i++){
		vector<Mat> vecCNN;
		rectNumRegion = Rect(nStartX+i*NUM_WIDTH, 0, NUM_WIDTH, NUM_HEIGHT);
		Mat imgNum = img(rectNumRegion);
		resize(imgNum, imgNum, Size(32, 32), 0, 0, INTER_CUBIC);
		imgNum.convertTo(imgNum, CV_64FC1, 1.0 / 255, 0);
		vecCNN.push_back((imgNum - 0.5));

		Mat results ,imgScore;
		cnnScore(vecCNN, results, imgScore);
		results.convertTo(results, CV_32SC1);

		int resultnum = results.at<int>(0);
		double dResultScore = imgScore.at<double>(0);
		cout << "\nclassification result: " << resultnum + 1 << " score:  " << dResultScore<< endl;

	}

	return 0;

}