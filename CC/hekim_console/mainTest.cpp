#include <cv.h>
#include <highgui.h>
#include <iostream>

#include "plateRecog.h"

using namespace cv;
using namespace std;

#define VIDEO 0

VideoCapture				m_vCapVideo;
Size2i						sizeInputVideo;

bool LoadVideoFrame(Mat& imgInput){
	m_vCapVideo.read(imgInput);
	if (!imgInput.data){
		printf("End of the video file\n");
		return false;
	}
	return true;
}

void SetVideoFile(const string& strVideoPath){
	m_vCapVideo.open(/*"sample/back.mp4"*/strVideoPath);
	if (!m_vCapVideo.isOpened()){
		printf("ERROR: Could not open the video file!\n");
		exit(-1);
	}
}

int main()
{
	int nCnt = 0;
	char cNum[100];

#if VIDEO
	SetVideoFile("sample/back.mp4");
	char imgName[100] = "kv30l_";
#endif

	//VideoCapture cap(0);
	//if (!cap.isOpened()){
	//	cout << "[ERROR]  could not open camera" << endl;
	//	return 0;
	//}

	Mat imgSrc, imgGray, imgDst;

	char Xmlfilename[100] = "train_cnn_svhn.xml";
	initRead(Xmlfilename);

	while (true){

#if(!VIDEO)
		sprintf(cNum, "sample_png/kv30l_%d.bmp", nCnt);
		if (nCnt == 289) break;
		imgSrc = imread(cNum);
#endif
		vector<Rect> vec_rectCandidate;
		Rect ROI;
		vector<Rect> vec_rectPlateRegion;
		vector<int> vec_nPlateNumRst;
		vector<double> vec_nPlateNumScore;

		bool bFrameValid = true;
#if VIDEO
		if (!LoadVideoFrame(imgSrc)){
			bool bFrameValid = false;
			break;
		}
#endif
		if (bFrameValid){
			char strPlateNum[4];
			PlateNumberRecognition(imgSrc, strPlateNum);
			cvWaitKey(1);
		}
		nCnt++;
	}
	return 0;
}

