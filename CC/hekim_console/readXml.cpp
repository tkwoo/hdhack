#include "readXml.h"
using namespace cv;
using namespace std;
//
//std::vector<ConvLayerConfig> convConfig;
//std::vector<FullConnectLayerConfig> fcConfig;
//SoftmaxLayerConfig softmaxConfig;
//vector<int> sample_vec;


void writexml(vector<Cvl> &CLayers, vector<Fcl> &hLayers, Smr &smr, char *xmlfile)
{
	FileStorage fs(xmlfile, FileStorage::WRITE);
	fs << "Hlayers" << "[";
	for (int i = 0; i < hLayers.size(); i++){
		fs << "{:";
		fs << "W" << hLayers[i].W;
		fs << "b" << hLayers[i].b;
		fs << "Wgrad" << hLayers[i].Wgrad;
		fs << "bgrad" << hLayers[i].bgrad;
		fs << "Wd2" << hLayers[i].Wd2;
		fs << "bd2" << hLayers[i].bd2;
		fs << "lr_b" << hLayers[i].lr_b;
		fs << "lr_w" << hLayers[i].lr_w;
		fs << "}";
	}
	fs << "]";
	fs << "CLayers" << "[";
	for (int cl = 0; cl < CLayers.size(); cl++){

		for (int i = 0; i < CLayers[cl].layer.size(); i++){
			fs << "{:";
			fs << "W" << CLayers[cl].layer[i].W;
			fs << "b" << CLayers[cl].layer[i].b;
			fs << "Wgrad" << CLayers[cl].layer[i].Wgrad;
			fs << "bgrad" << CLayers[cl].layer[i].bgrad;
			fs << "Wd2" << CLayers[cl].layer[i].Wd2;
			fs << "bd2" << CLayers[cl].layer[i].bd2;
			fs << "lr_b" << CLayers[cl].layer[i].lr_b;
			fs << "lr_w" << CLayers[cl].layer[i].lr_w;
			fs << "}";
		}

	}
	fs << "]";
	fs << "SMR" << "[";
	fs << "{:";
	fs << "W" << smr.W;
	fs << "}";
	fs << "{:";
	fs << "b" << smr.b;
	fs << "}";
	fs << "{:";
	fs << "Wgrad" << smr.Wgrad;
	fs << "}";
	fs << "{:";
	fs << "bgrad" << smr.bgrad;
	fs << "}";
	fs << "{:";
	fs << "cost" << smr.cost;
	fs << "}";
	fs << "{:";
	fs << "Wd2" << smr.Wd2;
	fs << "}";
	fs << "{:";
	fs << "bd2" << smr.bd2;
	fs << "}";
	fs << "{:";
	fs << "lr_b" << smr.lr_b;
	fs << "}";
	fs << "{:";
	fs << "lr_w" << smr.lr_w;
	fs << "}";
	fs << "]";

	fs.release();
}

void readxml(vector<Cvl> &CLayers, vector<Fcl> &hLayers, Smr &smr, char *xmlfile)
{
	//vector<Fcl> hLayers2 = hLayers;

	FileStorage fs(xmlfile, FileStorage::READ);
	FileNode hlayers = fs["Hlayers"];
	FileNodeIterator it = hlayers.begin(), it_end = hlayers.end();
	int i = 0;

	for (; it != it_end; ++it, i++){
		std::cout << "hlayers #" << i << ": ";
		(*it)["W"] >> hLayers[i].W;
		(*it)["b"] >> hLayers[i].b;
		(*it)["Wgrad"] >> hLayers[i].Wgrad;
		(*it)["bgrad"] >> hLayers[i].bgrad;
		(*it)["Wd2"] >> hLayers[i].Wd2;
		(*it)["bd2"] >> hLayers[i].bd2;
		(*it)["lr_b"] >> hLayers[i].lr_b;
		(*it)["lr_w"] >> hLayers[i].lr_w;

	}

	i = 0;
	FileNode clayers = fs["CLayers"];
	it = clayers.begin(), it_end = clayers.end();

	for (int i = 0; i < CLayers.size(); i++){

		std::cout << "clayers #" << i << ": ";

		for (int j = 0; j < CLayers[i].layer.size(); j++){
			(*it)["W"] >> CLayers[i].layer[j].W;
			(*it)["b"] >> CLayers[i].layer[j].b;
			(*it)["Wgrad"] >> CLayers[i].layer[j].Wgrad;
			(*it)["bgrad"] >> CLayers[i].layer[j].bgrad;
			(*it)["Wd2"] >> CLayers[i].layer[j].Wd2;
			(*it)["bd2"] >> CLayers[i].layer[j].bd2;
			(*it)["lr_b"] >> CLayers[i].layer[j].lr_b;
			(*it)["lr_w"] >> CLayers[i].layer[j].lr_w;

			++it;
		}
	}


	FileNode Smr = fs["SMR"];
	it = Smr.begin(), it_end = Smr.end();

	(*it)["W"] >> smr.W;
	it++;
	(*it)["b"] >> smr.b;
	it++;
	(*it)["Wgrad"] >> smr.Wgrad;
	it++;
	(*it)["bgrad"] >> smr.bgrad;
	it++;
	(*it)["cost"] >> smr.cost;
	it++;
	(*it)["Wd2"] >> smr.Wd2;
	it++;
	(*it)["bd2"] >> smr.bd2;
	it++;
	(*it)["lr_b"] >> smr.lr_b;
	it++;
	(*it)["lr_w"] >> smr.lr_w;

	fs.release();
}