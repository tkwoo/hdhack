#include <iostream>
#include <stdlib.h>
#include <winsock2.h>
#include <process.h>
#include <vector>
#pragma comment(lib,"ws2_32.lib")
#define BUFSIZE 1024
#define PORT 2738
using namespace std;
typedef struct // socket info
{
	SOCKET C_Sock;
	SOCKADDR_IN clntAddr;
} PER_HANDLE_DATA, *LPPER_HANDLE_DATA;
typedef struct // buffer info
{
	OVERLAPPED overlapped; // 완료된 입출력 정보 얻기위해
	char buffer[BUFSIZE];
	WSABUF wsaBuf; // WSASend, WSARecv 함수의 인자로 전달되는 버퍼에 사용되는 구조체
} PER_IO_DATA, *LPPER_IO_DATA;
// 완료된 쓰레드에 관한 처리
unsigned int __stdcall CompletionThread(LPVOID pComPort);
void ErrorHandling(char *message);
vector<SOCKET> S_list;
vector<string> list;
char *client_ID, *client_State, *client_DataType, *client_Data;
string C_ID, C_State, C_DataType, C_data;
bool check = true;
int main()
{
	WSADATA wsaData;
	HANDLE H_CompPort; // 만들어질 CompletionPort가 전달될 Handle
	SYSTEM_INFO SystemInfo; // 쓰레드를 생성할때 CPU 의 개수에 따라 쓰레드를 만들어 줘야 하기 때문에 시스템 정보 필요
	SOCKADDR_IN servAddr;
	LPPER_IO_DATA PerIoData;
	LPPER_HANDLE_DATA PerHandleData;
	SOCKET S_Sock;
	int RecvBytes;
	int i, Flags;
	int N_Cnt = 0; // 접속된 클라이언트 수
	string Client_list;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) // Load Winsock 2.2 DLL
		ErrorHandling("WSAStartup() error!");
	// Completion Port 생성
	H_CompPort = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0);
	GetSystemInfo(&SystemInfo);
	// Completion Port 에서 입출력 완료를 대기하는 쓰레드를 CPU 개수만큼 생성
	for (i = 0; i<SystemInfo.dwNumberOfProcessors; i++)
		_beginthreadex(NULL, 0, CompletionThread, (LPVOID)H_CompPort, 0, NULL);
	S_Sock = WSASocket(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (S_Sock == INVALID_SOCKET)
	{
		cout << "WSASocket call failed with error: " << WSAGetLastError() << endl;
		closesocket(S_Sock);
		WSACleanup();
		return false;
	}
	servAddr.sin_family = AF_INET;
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servAddr.sin_port = htons(PORT);
	if (bind(S_Sock, (SOCKADDR*)&servAddr, sizeof(servAddr)) == SOCKET_ERROR)
	{
		cout << "bind failed with error: " << WSAGetLastError() << endl;
		closesocket(S_Sock);
		WSACleanup();
		return false;
	}
	if (listen(S_Sock, 5) == SOCKET_ERROR)
	{
		cout << "listen failed with error: " << WSAGetLastError() << endl;
		closesocket(S_Sock);
		WSACleanup();
		return false;
	}
	cout << "Server is running..." << endl;
	cout << "--------------------" << endl;
	while (TRUE)
	{
		SOCKET C_Sock;
		SOCKADDR_IN clntAddr;
		int addrLen = sizeof(clntAddr);
		C_Sock = accept(S_Sock, (SOCKADDR*)&clntAddr, &addrLen);
		if (C_Sock == INVALID_SOCKET)
		{
			cout << "accept failed with error: " << WSAGetLastError() << endl;
			closesocket(C_Sock);
			WSACleanup();
			return false;
		}
		cout << "Client access | ip = " << inet_ntoa(clntAddr.sin_addr) << " port = " << ntohs(clntAddr.sin_port) << endl;
		N_Cnt++;
		cout << "현재 접속자 수 : " << N_Cnt << endl;
		// 연결된 클라이언트의 정보들을 저장
		PerHandleData = (LPPER_HANDLE_DATA)malloc(sizeof(PER_HANDLE_DATA));
		PerHandleData->C_Sock = C_Sock;
		memcpy(&(PerHandleData->clntAddr), &clntAddr, addrLen);

		// Overlapped 소켓과 CompletionPort 연결
		CreateIoCompletionPort((HANDLE)C_Sock, H_CompPort, (DWORD)PerHandleData, 0);
		// 연결된 클라이언트를 위한 버퍼 설정, OVERLAPPED 구조체 변수 초기화
		PerIoData = (LPPER_IO_DATA)malloc(sizeof(PER_IO_DATA));
		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;
		Flags = 0;
		// overlapped data Recv
		WSARecv(PerHandleData->C_Sock, &(PerIoData->wsaBuf), 1, (LPDWORD)&RecvBytes,
			(LPDWORD)&Flags, &(PerIoData->overlapped), NULL);
	}
	return 0;
}
// 입출력 완료에 따른 쓰레드의 행동 정의
unsigned int __stdcall CompletionThread(LPVOID pComPort)
{
	HANDLE H_CompPort = (HANDLE)pComPort;
	DWORD BytesTransferred;
	LPPER_HANDLE_DATA PerHandleData;
	LPPER_IO_DATA PerIoData;
	DWORD flags;
	WSABUF Tmp_buff;
	char RES[1024] = "";
	char STOP_RES[1024] = "";
	WSAOVERLAPPED overlapped;
	while (1)
	{
		char client_info[1024] = "";
		// 입출력이 완료된 소켓의 정보 얻음
		GetQueuedCompletionStatus(H_CompPort, &BytesTransferred, (LPDWORD)&PerHandleData,
			(LPOVERLAPPED*)&PerIoData, INFINITE);

		if (BytesTransferred == 0) // EOF 전송시
		{
			closesocket(PerHandleData->C_Sock);
			free(PerHandleData);
			free(PerIoData);
			continue;
		}
		char port[10];
		PerIoData->wsaBuf.buf[BytesTransferred] = '\0';
		itoa(PerHandleData->clntAddr.sin_port, port, 10);
		cout << "Recv[" << PerIoData->wsaBuf.buf << "]" << endl;
		cout << "port : " << PerHandleData->clntAddr.sin_port << endl;
		//cout << port << endl;
		client_ID = strtok_s(PerIoData->wsaBuf.buf, "/", &client_State);
		client_State = strtok_s(client_State, "/", &client_DataType);
		client_DataType = strtok_s(client_DataType, "/", &client_Data);
		if (strlen(client_Data) > 1)
		{
			client_Data = strtok_s(client_Data, "/", &client_Data);
			C_ID = client_ID; C_State = client_State; C_DataType = client_DataType; C_data = client_Data;
			if (C_DataType == "REG_NUM") // Client info 등록
			{
				cout << "client " << client_ID << " registration complete." << endl;
				list.push_back(client_Data);
				S_list.push_back(PerHandleData->C_Sock);
			}
			else if (C_DataType == "REQ") // 번호판 인식된 차량과의 연결 요청
			{
				cout << "client_ data is : " << client_Data << endl;
				bool check = true;
				for (int x = 0; x < list.size(); x++)
				{
					if (C_data == list[x])
					{
						/*strcat(RES, client_ID); strcat(RES, "/");
						strcat(RES, client_State); strcat(RES, "/");
						strcat(RES, client_DataType); strcat(RES, "/");
						strcat(RES, client_Data);*/

						Tmp_buff.buf = "RES";
						Tmp_buff.len = strlen(Tmp_buff.buf);

						WSASend(S_list[x], &Tmp_buff, 1, NULL, 0, NULL, NULL);
						cout << " Request complete ( " << client_ID << " --> " << client_Data << " )" << endl;
						cout << "send data is : " << Tmp_buff.buf << endl;
						check = false; break;
					}
				}
				if (check == true) // server에 해당 차번호가 없으면
				{
					strcat(client_info, client_ID); strcat(client_info, "/");
					strcat(client_info, client_State); strcat(client_info, "/");
					strcat(client_info, "REJECT/No_Vehicle");
					Tmp_buff.buf = client_info;
					WSASend(PerHandleData->C_Sock, &Tmp_buff, 1, NULL, 0, NULL, NULL);
					cout << " Can't find a requested Vehicle. " << endl;
				}
			}
			else if (C_DataType == "ANS")
			{
				cout << " Received obstacle data. " << endl;
				//WSASend(PerHandleData->C_Sock, &(PerIoData->wsaBuf), 1, NULL, 0, &(PerIoData->overlapped), NULL);
			}
			else if (C_DataType == "REJECT"){
				cout << " Recheck client number --> " << client_ID << endl;
				//WSASend(PerHandleData->C_Sock, &(PerIoData->wsaBuf), 1, NULL, 0, &(PerIoData->overlapped), NULL);
			}
			else if (C_DataType == "STOP")
			{
				cout << " STOP REQ " << endl;
				for (int x = 0; x < list.size(); x++)
				{
					if (C_data == list[x])
					{
						/*strcat(STOP_RES, client_ID); strcat(STOP_RES, "/");
						strcat(STOP_RES, client_State); strcat(STOP_RES, "/");
						strcat(STOP_RES, client_DataType); strcat(STOP_RES, "/");
						strcat(STOP_RES, client_Data);*/

						Tmp_buff.buf = "STOP";
						Tmp_buff.len = strlen(Tmp_buff.buf);
						WSASend(S_list[x], &Tmp_buff, 1, NULL, 0, NULL, NULL);
						cout << " STOP Request complete ( " << client_ID << " --> " << client_Data << " )" << endl;
						cout << "send STOP data is : " << Tmp_buff.buf << endl;
						break;
					}
				}
			}
			else
				cout << "--- wrong Data_type ---" << endl;
		}
		else
			cout << " Command error. (No data in Socket) " << endl;
		// send
		//PerIoData->wsaBuf.len = BytesTransferred;
		//WSASend(PerHandleData->C_Sock, &(PerIoData->wsaBuf), 1, NULL, 0, NULL, NULL);
		// RECEIVE AGAIN
		memset(&(PerIoData->overlapped), 0, sizeof(OVERLAPPED));
		PerIoData->wsaBuf.len = BUFSIZE;
		PerIoData->wsaBuf.buf = PerIoData->buffer;
		flags = 0;
		WSARecv(PerHandleData->C_Sock, &(PerIoData->wsaBuf), 1, NULL, &flags, &(PerIoData->overlapped), NULL);
	}
	return 0;
}
void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}