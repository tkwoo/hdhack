#include <iostream>
#include <stdlib.h>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")
#define ID 1234
using namespace std;

void ErrorHandling(char *message);

int main()
{
	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN recvAddr;
	WSABUF dataBuf;
	WSABUF temp_Buf;
	char *client_ID, *client_State, *client_DataType, *client_Data;
	string C_ID, C_State, C_DataType, C_data;
	char message[1024] = "";
	char ID1[8] = { 0, };
	char State[1024] = "";
	char data[10000] = "";
	char REG_NUM[1024] = "1234/";
	char REQ[1024] = "1234/";
	int sendBytes = 0;
	int recvBytes = 0;
	int flags = 0;
	itoa(ID, ID1, 10);
	WSAEVENT event;
	WSAOVERLAPPED overlapped;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) // Load Winsock 2.2 DLL
		ErrorHandling("WSAStartup() error!");

	hSocket = WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSocket == INVALID_SOCKET)
		ErrorHandling("socket() error");

	memset(&recvAddr, 0, sizeof(recvAddr));
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	recvAddr.sin_port = htons(atoi("2738"));
	if (connect(hSocket, (SOCKADDR*)&recvAddr, sizeof(recvAddr)) == SOCKET_ERROR)
		ErrorHandling("connect() error!");

	cout << "**** Client B test ****" << endl;
	cout << "**** ID : " << ID << " ****" << endl;
	// 구조체에 이벤트핸들 삽입해서 전달
	event = WSACreateEvent();
	memset(&overlapped, 0, sizeof(overlapped));
	overlapped.hEvent = event;

	strcat(REG_NUM, "000/REG_NUM/1234");

	dataBuf.len = strlen(REG_NUM);
	dataBuf.buf = REG_NUM;
	if (WSASend(hSocket, &dataBuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
			ErrorHandling("WSASend() error");
	}
	while (true)
	{
		flags = 0;
		//cout << " Send data : (exit : q)" << endl;
		//scanf("%s", message);
		if (!strcmp(message, "q")) break;
		dataBuf.len = 1000;
		dataBuf.buf = message;

		WSARecv(hSocket, &dataBuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL);
		string temp;
		temp = dataBuf.buf;
		if (temp == "RES")
		{
			char ANS_Env[1024] = "";
			strcat(ANS_Env, ID1); strcat(ANS_Env, "/0,0,0/ANS/000111");
			temp_Buf.buf = ANS_Env;
			temp_Buf.len = strlen(temp_Buf.buf);
			if (WSASend(hSocket, &temp_Buf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
			{
				if (WSAGetLastError() != WSA_IO_PENDING)
					ErrorHandling("WSASend() error");
			}
			else
				cout << temp_Buf.buf << endl;

			//Sleep(100);		
		}
	}
	closesocket(hSocket);
	WSACleanup();
	return 0;
}

void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}
