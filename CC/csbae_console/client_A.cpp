#include <iostream>
#include <stdlib.h>
#include <winsock2.h>
#pragma comment(lib,"ws2_32.lib")
#define ID 1111
using namespace std;

//hekim added
#include "../hekim_console/plateRecog.h"

VideoCapture				m_vCapVideo;
Size2i						sizeInputVideo;
/////////////

void ErrorHandling(char *message);
bool LoadVideoFrame(Mat& imgInput);
void SetVideoFile(const string& strVideoPath);

int main()
{
	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN recvAddr;
	WSABUF dataBuf;
	WSABUF Tmp_buff;
	char message[1024] = { 0, };
	char ID1[8] = { 0, };
	char State[1024] = "";
	char data[10000] = "";
	char REG_NUM[1024] = "1111/";
	char REQ[1024] = "1111/";
	int sendBytes = 0;
	int recvBytes = 0;
	int flags = 0;

	WSAEVENT event;
	WSAOVERLAPPED overlapped;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) // Load Winsock 2.2 DLL
		ErrorHandling("WSAStartup() error!");

	hSocket = WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSocket == INVALID_SOCKET)
		ErrorHandling("socket() error");

	memset(&recvAddr, 0, sizeof(recvAddr));
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	recvAddr.sin_port = htons(atoi("2738"));
	if (connect(hSocket, (SOCKADDR*)&recvAddr, sizeof(recvAddr)) == SOCKET_ERROR)
		ErrorHandling("connect() error!");

	// 구조체에 이벤트핸들 삽입해서 전달
	event = WSACreateEvent();
	memset(&overlapped, 0, sizeof(overlapped));
	overlapped.hEvent = event;

	cout << "**** Client A test ****" << endl;
	cout << "**** ID : " << ID << " ****" << endl;

	strcat(REG_NUM, "000/REG_NUM/1111");

	//scanf("%s", message);
	dataBuf.len = strlen(REG_NUM);
	dataBuf.buf = REG_NUM;
	if (WSASend(hSocket, &dataBuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
	{
		if (WSAGetLastError() != WSA_IO_PENDING)
			ErrorHandling("WSASend() error");
	}

	////////Plate Number Recognition//////////////////////////////////////
	Mat imgSrc, imgGray, imgDst;

	char Xmlfilename[100] = "../hekim_console/train_cnn_svhn.xml";
	initRead(Xmlfilename);
	
	int nCnt = 0;
	char cNum[100];

	while (true)
	{
		vector<Rect> vec_rectCandidate;
		Rect ROI;
		vector<Rect> vec_rectPlateRegion;
		vector<int> vec_nPlateNumRst;
		vector<double> vec_nPlateNumScore;
		bool bFrameValid = true;

		sprintf(cNum, "../hekim_console/sample_png/kv30l_%d.bmp", nCnt);
		if (nCnt == 289) break;
		imgSrc = imread(cNum);

		if (bFrameValid){
			char strPlateNum[4];

			Mat imgAdapt;
			float fResizeRatio = 640 / (float)imgSrc.cols;
			resize(imgSrc, imgSrc, Size(imgSrc.cols*fResizeRatio, imgSrc.rows*fResizeRatio));

			cvtColor(imgSrc, imgGray, CV_BGR2GRAY);

			imgDst = imgSrc.clone();

			GenerateCandidate(imgGray, vec_rectCandidate);
			//cout << "check" << endl;
			ClusterPlateRegion(vec_rectCandidate, ROI, vec_rectPlateRegion);
			//cout << "check" << endl;
			adaptiveThreshold(imgGray, imgAdapt, 250, ADAPTIVE_THRESH_MEAN_C, // Adaptive 함수
				THRESH_BINARY, // 이진화 타입
				/*blockSize*/11,  // 이웃크기
				/*threshold*/10); // threshold used

			imgAdapt = abs(imgAdapt - 255);

			RecognizePlateNumber(imgAdapt, ROI, vec_rectPlateRegion, vec_nPlateNumRst, vec_nPlateNumScore);
			VisualizeRegion(imgDst, vec_rectPlateRegion, ROI, vec_nPlateNumRst, vec_nPlateNumScore);
			ReturnPlateNumStr(vec_nPlateNumRst, strPlateNum);
		}
		nCnt++;

		flags = 0;
		cout << " Send data : (exit : q)" << endl;
		cin >> message;

		if (!strcmp(message, "q")) break;
		dataBuf.len = 1000;
		dataBuf.buf = message;
		if (WSASend(hSocket, &dataBuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR)
		{
			if (WSAGetLastError() != WSA_IO_PENDING)
				ErrorHandling("WSASend() error");
		}

		WSAWaitForMultipleEvents(1, &event, TRUE, WSA_INFINITE, FALSE);
		Tmp_buff.len = strlen(message);
		Tmp_buff.buf = message;
		WSARecv(hSocket, &Tmp_buff, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL);
		char * COM_TYPE;
		char * COM_data;
		COM_TYPE = strtok_s(Tmp_buff.buf, "/", &COM_data);
		COM_data = strtok_s(COM_data, "/", &COM_data);
		if (COM_TYPE == "RES_ENV")
			cout << "Recv[" << COM_data << "]" << endl;
	}
	closesocket(hSocket);
	WSACleanup();
	return 0;
}

void ErrorHandling(char *message)
{
	fputs(message, stderr);
	fputc('\n', stderr);
	exit(1);
}


//hekim added
bool LoadVideoFrame(Mat& imgInput){
	m_vCapVideo.read(imgInput);
	if (!imgInput.data){
		printf("End of the video file\n");
		return false;
	}
	return true;
}

void SetVideoFile(const string& strVideoPath){
	m_vCapVideo.open(/*"sample/back.mp4"*/strVideoPath);
	if (!m_vCapVideo.isOpened()){
		printf("ERROR: Could not open the video file!\n");
		exit(-1);
	}
}
//////////////