/**
@file DefStruct.h
@date 2016/05/13
@author 우태강(tkwoo@inha.edu) Inha Univ.
@brief 전체 파라미터 통일을 위한 Struct 및 변수 정의 file
*/

#pragma once
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#define PI 3.141592

#ifndef WIN32
#define sprintf_s snprintf
#endif

using namespace cv;
using namespace std;

enum { Daimler, KITTI, HICAM, CityScape };
enum { Ped, Car, TrafficSign, TrafficLight, Else};
enum { STEREO_BM = 0 }; //, STEREO_SGBM = 1
enum { GRAY, COLOR };
enum { CLI_REQ, SERV_REQ, ANS, REJECT };

struct stixel_t
{
	int nGround;		///< Ground bound point
	int nHeight;		///< Upper bound point
	uchar chDisparity;	///< Disparity(normalized 255)
	int nCol;			///< column
	double dZ;			///< distance(meter)
	double dY;			///< 
	double dX;			///< 
	stixel_t(){
		nGround = -1;
		nHeight = -1;
		chDisparity = 0;
		dZ = 0.;
		dY = 0.;
		dX = 0.;
	}
};

struct SVehicleInfo{
	double dGPSX;
	double dGPSY;
	int nVelo;
	double dSteering;
};

struct SObjectHD{
	//unit : meter all
	double dZ;
	double dX;
	double dWidth;
	int nClass;
	SObjectHD(){
		dZ = 0.;
		dX = 0.;
		dWidth = 0.;
		nClass = Car;
	}
};

struct Object_t
{
	cv::Rect rectBB;
	double dZ;
	std::vector<stixel_t> vecobjStixels;
	int nClass;
	double dClassScore;
	double dCollisionRisk;
	Object_t(){
		rectBB = cv::Rect(0, 0, 0, 0);
		dZ = 0.;
		vecobjStixels.clear();
		nClass = Else;
		dClassScore = 0.;
		dCollisionRisk = 1.;
	}
	Object_t(cv::Rect rect, double dz){
		nClass = Else;
		dClassScore = 0.;
		dCollisionRisk = 1.;
		rectBB = rect; dZ = dz;
		vecobjStixels.clear();
	}
	Object_t(cv::Rect rect, stixel_t objStixel){
		nClass = Else;
		dClassScore = 0.;
		dCollisionRisk = 1.;
		rectBB = rect; dZ = objStixel.dZ;
		vecobjStixels.push_back(objStixel);
	}
};

// TBD

struct SLane
{
	cv::Point2d ptUvStartLine; ///< 영상좌표계에서의 차선 시작점
	cv::Point2d ptUvEndLine; ///< 영상좌표계에서의 차선 종료점
	SLane(){
		ptUvEndLine = Point(0, 0);
		ptUvStartLine = Point(0, 0);
	}
};
struct SLanes
{
	// new IF
	vector<SLane> vecLanesLeft;
	vector<SLane> vecLanesRight;

	// old IF
	vector<SLane> vecLanes;
	/**
	@author 이영완
	@date 2015.12.17
	@param vecPt : input points
	@param nPtNum : the number of points
	@brief 넣어준 점의 개수에 따라 polynomial regression을 통해 곡선함수의 계수를 찾음.
	@brief 점3개--> 2차함수, 점4개면 --> 3차함수를 그릴 수 있다.
	@brief 점을 넣어주는 순서가 중요. x좌표의 오름차순으로. ex) (1,1) --> (10,50) --> (50,10)
	@return 곡선함수의 계수를 Mat으로 반환
	*/
	Mat GetCurveCoeff(vector<SLane>& vecobjLanes)
	{
		int nNumVar = vecobjLanes.size() * 2;
		int nOrder = nNumVar - 1;
		Mat_<float> src_x = Mat::zeros(nNumVar, 1, CV_32FC1);
		Mat_<float> src_y = Mat::zeros(nNumVar, 1, CV_32FC1);

		for (int i = 0; i < nNumVar; i++)
		{
			if (i % 2 == 0){
				src_x.at<float>(i) = (float)vecobjLanes[i].ptUvStartLine.x;
				src_y.at<float>(i) = (float)vecobjLanes[i].ptUvStartLine.y;
			}
			else{
				src_x.at<float>(i) = (float)vecobjLanes[i].ptUvEndLine.x;
				src_y.at<float>(i) = (float)vecobjLanes[i].ptUvEndLine.y;
			}
		}

		Mat resultCoef = Mat::zeros(nNumVar, 1, CV_32FC1);
		//polyfit(src_x, src_y, resultCoef, nOrder);
		//cv::polyfit(src_y, src_x, resultCoef, nOrder);

		//draw
		//for (int i = vecPt[0].x; i <= vecPt[2].x; i++)
		//{
		//	float solv_y = /*resultCoef.at<float>(3)*pow(i, 3) +*/ resultCoef.at<float>(2)*pow(i, 2) + resultCoef.at<float>(1)*i + resultCoef.at<float>(0);
		//	printf("part_3 : (%d,%d)\n", i, (int)solv_y);
		//	circle(matCanvas, Point(i, (int)solv_y), 1, Scalar(0, 255, 0), 1, 8, 0);
		//}

		return resultCoef;
	}
	SLanes(){
		vecLanes.clear();
		vecLanesLeft.clear();
		vecLanesRight.clear();
	}

};

struct CameraParam_t
{
	double m_dPitchDeg;	///< unit : degree
	double m_dYawDeg; ///< unit : degree
	double m_dFocalLength; ///< unit : pixels
	double m_dOx; ///< unit : pixels
	double m_dOy; ///< unit : pixels
	double m_dCameraHeight; ///< cam height from ground
	double m_dFOVvDeg; ///< vertical FOV
	double m_dFOVhDeg; ///< horizontal FOV
	int m_nVanishingY; ///< vanishing line location
	cv::Size m_sizeSrc;
	CameraParam_t(){
		m_dPitchDeg = 0.;
		m_dYawDeg = 0.;
		m_dFocalLength = 0.;
		m_dOx = 0.;
		m_dOy = 0.;
		m_dCameraHeight = 0.;
		m_dFOVvDeg = 0.;
		m_dFOVhDeg = 0.;
		m_sizeSrc = cv::Size(0, 0);
	}
};
struct StereoCamParam_t
{
	int m_nNumberOfDisp; ///< number of disparity.
	int m_nWindowSize; ///< window size. It must be odd number.
	double m_dBaseLine; ///< baseline, unit : meters1
	double m_dMaxDist; ///< Maximum distance value, unit : meters
	CameraParam_t objCamParam;
	StereoCamParam_t(){
		m_nNumberOfDisp = 80;
		m_nWindowSize = 9;
		m_dBaseLine = 0.;
		m_dMaxDist = 50.0;
	}
};

